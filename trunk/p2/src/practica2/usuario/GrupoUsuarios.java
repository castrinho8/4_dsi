package practica2.usuario;

import java.util.ArrayList;

/**
 * Entidad que modela los grupos de usuarios.
 */
public class GrupoUsuarios implements Usuario {

	private String name;
	private int id;
	private ArrayList<UsuarioIndividual> users;
	private static int _ultimoID = 0;
	
	/**
    * Constructor de la clase que inicializa los atributos del grupo.
    * @param name Nombre del grupo.
	*/	
	public GrupoUsuarios(String name) {
		super();
		this.name = name;
		this.id = GrupoUsuarios.getSiguienteId();
		this.users = new ArrayList<UsuarioIndividual>();
	}

	/**
    * Funcion que devuelve el siguiente numero de id disponible para un grupo.
    * @return Siguiente id para un grupo.
	*/
	private static int getSiguienteId() {
		return _ultimoID++;
	}

	/**
    * Funcion que anhade un usuario al grupo.
	* @param u Usuario a anhadir.
    * @return True si se anhade correctamente y False si el usuario ya esta en un grupo.
	*/		
	public boolean anhadirUsuario(UsuarioIndividual u){
		if(u.has_group())
			return false;
		this.users.add(u);
		u.set_group(this);
		return true;
	}

	/**
    * Funcion que elimina un usuario de un grupo.
	* @param u Usuario a eliminar del grupo.
    * @return True si se elimina correctamente y False en caso contrario.
	*/				
	public boolean eliminarUsuario(UsuarioIndividual u){
		if(!this.users.remove(u))
			return false;
		u.set_group(null);
		return true;
	}

	/**
    * Getter del nombre del grupo.
    * @return Nombre del grupo.
	*/
	@Override
	public String obtenerNombre() {
		return this.name;
	}

	/**
    * Getter del ID del grupo.
    * @return El id del grupo.
	*/
	@Override
	public int obtenerID() {
		return this.id;
	}

	/**
    * Funcion que comprueba si el usuario introducido por parametro esta en el grupo.
	* @param otroUsuario El usuario a comprobar su autorizacion.
    * @return True si se encuentra y False en caso contrario.
	*/
	@Override
	public boolean autorizar(Usuario otroUsuario) {
		boolean res = false;
		for(UsuarioIndividual u : this.users)
			res |= u.autorizar(otroUsuario);
		return res;
	}

}
