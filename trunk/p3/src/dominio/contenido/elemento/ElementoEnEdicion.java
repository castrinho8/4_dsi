package dominio.contenido.elemento;

import dominio.usuario.Usuario;

/**
 * Clase que modela los elementos que se encuentran en edición.
 * Subclase de SituacionElemento.
 * @see SituacionElemento
 */
public class ElementoEnEdicion extends SituacionElemento {
	
	/**
	 * Constructor de la clase ElementoEnEdicion.
	 * Inicializa al Usuario pasado por parámetro como el editor autorizado
	 * de ese elemento.
	 * @param editor El editor correspondiente a este elemento.
	 */
	public ElementoEnEdicion(Usuario editor) {
		_editor = editor;
	}
	
	/**
	 * Método que cancela la edición del elemento introducido como parámetro.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha cancelación.
	 * @param elemento El elemento a cancelar su edición.
	 * @param editor El editor que realiza la cancelación.
	 * @throws OperacionInvalida  En caso de que no se disponga de permisos para realizar la cancelación.
	 */
	public void cancelarEdicion(Elemento elemento, Usuario editor)
	throws OperacionInvalida {
		if (_editor.equals(editor)) {
			elemento.establecerSituacion(new ElementoVisible());
		} else {
			throw new OperacionInvalida("No tiene permisos para ejecutar esta operación");
		}
	}
	
	/**
	 * Método que realiza una proposición para que el elemento pasado por parámetro, sea revisado.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha proposición.
	 * @param elemento El elemento a proponer para revisión.
	 * @param editor El editor que realiza la proposición.
	 * @param revision Revisión a realizar.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la revisión.
	 */
	public void proponerRevision(Elemento elemento, Usuario editor,	String revision)
	throws OperacionInvalida {
		if (_editor.equals(editor)) {
			elemento.establecerSituacion(new ElementoPendienteAprobacion(elemento, _editor, revision));
			elemento.aprobar(editor);
		} else {
			throw new OperacionInvalida("No tiene permisos para ejecutar esta operación");
		}
	}
	
	/**
	 * Método que devuelve el codigo que indica la situación en la que se encuentra
	 * el elemento.
	 * @param elemento El elemento.
	 * @return El codigo de situacion, en este caso "ED"
	 */
	public String codigoSituacion(Elemento elemento) {
		return "ED";
	}
	
	/**
	 * Método que devuelve la descripción de la situación.
	 * @return La descripción de la situación, en este caso "en edición"
	 */
	protected String descripcion() {
		return "en edición";
	}
	
	private Usuario _editor;
	
}
