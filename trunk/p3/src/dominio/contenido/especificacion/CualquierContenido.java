package dominio.contenido.especificacion;

import dominio.contenido.Contenido;

/**
 * Clase que modela el comportamiento de una especificacion de un contenido cualquiera.
 *
 */
public class CualquierContenido implements EspecificacionContenido {
	
	/**
	 * Método que comprueba si el contenido es válido.
	 * (La implementación devuelve True siempre) 
	 * @param contenido El contenido a comprobar.
	 * @return True.
	 */
	public boolean valido(Contenido contenido) {
		return true;
	}
	
	/**
	 * Método que devuelve la descripción del contenido
	 * La implementacion por defecto devuelve el string "Todos los contenidos".
	 * @return La descripcion.
	 */
	public String descripcion() {
		return "Todos los contenidos";
	}
	
	/**
	 * Método que devuelve el toString formado únicamente por la descripción.
	 * @return Un String con la descripción del contenido.
	 */
	public String toString() {
		return descripcion();
	}
	
}
