package dominio.contenido;

import util.Interesante;
import dominio.contenido.elemento.Elemento;
import dominio.usuario.Usuario;

/**
 * Clase abstracta que representa un contenido del sistema.
 * Superclase de Interesante, Elemento y ComplementoContenido.
 * @see Interesante
 * @see Elemento
 * @see ComplementoContenido
 * 
 */
public abstract class Contenido extends Interesante {
	
	/**
	 * Getter del propietario del contenido.
	 * @return El propietario del contenido. Null en este caso.
	 */
	public Usuario obtenerPropietario() {
		return null;
	}
	
	/**
	 * Setter del propietario del contenido.
	 * @param propietario Usuario propietario del contenido.
	 */
	public void establecerPropietario(Usuario propietario) {	}
	
	/**
	 * Getter del título del contenido.
	 * @return el título del contenido. Null en este caso.
	 */
	public String obtenerTitulo() {
		return null;
	}
	
	/**
	 * Setter del título del contenido.
	 * @param titulo Nuevo título del contenido.
	 */
	public void establecerTitulo(String titulo) {	}
	
	/**
	 * Método que devuelve una versión imprimible del contenido.
	 * @return Un string que representa una version imprimible del contenido.
	 */
	public String obtenerVersionImprimible() {
		return obtenerVersionImprimible("");
	}
	
	/**
	 * Método que devuelve una versión imprimible de una sección del contenido.
	 * @param seccion Sección de la que se quiere obtener su version imprimible.
	 * @return Versión imprimible de la sección.
	 */
	public abstract String obtenerVersionImprimible(String seccion);
	
	/**
	 * Método que permite obtener la raiz del contenido.
	 * @return El elemento raiz de este contenido.
	 */
	public abstract Elemento obtenerRaiz();
	
	/**
	 * Getter del texto del contenido.
	 * @return El texto del contenido.
	 */
	public abstract String obtenerTexto();
	
	/**
	 * Setter del texto del contenido.
	 * @param texto Nuevo texto del contenido.
	 */
	public abstract void establecerTexto(String texto);
	
	/**
	 * Método que comprueba si un usuario ha valorado o no el contenido.
	 * @param usuario Usuario que se desea comprobar.
	 * @return Un booleano que indica si el usuario ha valorado o no el contenido.
	 */
	public abstract boolean valorado(Usuario usuario);
	
	/**
	 * Método que devuelve la valoración del contenido.
	 * @return un entero que especifica la valoracion del contenido.
	 */
	public abstract int obtenerValoracion();

	/**
	 * Método que devuelve una copia del contenido.
	 * @return Una instancia de contenido con las mismas caracteristicas que el actual.
	 */
	public abstract Contenido copiar();
	
	/**
	 * Método que copia el estado de un contenido en este objeto.
	 * @param copia Contenido del que copiar el estado.
	 */
	protected void copiarEstado(Contenido copia) {}
	
}
