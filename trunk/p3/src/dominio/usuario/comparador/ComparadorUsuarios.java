package dominio.usuario.comparador;

import java.util.Comparator;
import dominio.usuario.Usuario;

/**
 * Interfaz de comparadores de Usuarios.
 * Extiende la interfaz estandar Comparator.
 */
public interface ComparadorUsuarios extends Comparator<Usuario> {}
