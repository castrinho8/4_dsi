package practica2.servidor;

import practica2.usuario.Usuario;

public abstract class ServidorAbstracto implements Servidor {

	private Servidor maestro;
	
	/**
     * Obtiene el servidor maestro asociado con el servidor.
     *
     * @return servidor maestro. <code>null</code> si no tiene maestro
     */
	@Override
	public  Servidor obtenerMaestro() {
		return this.maestro;
	}

    /**
     * Establece el servidor maestro asociado con el servidor.
     *
     * @param maestro servidor maestro que será asociado
     */
	@Override
	public void establecerMaestro(Servidor maestro) {
		if(!this.check_relatives(maestro))
			this.maestro = maestro;
	}

    /**
     * Obtiene el nombre del servidor.
     *
     * @return nombre del servidor
     */
	@Override
	public abstract String obtenerNombre();


    /**
     * Servicio de resolución de nombres, devuelve la cadena de texto
     * asociada a una clave dada.
     *
     * @param usuario usuario que realiza la petición
     * @param clave nombre a ser resuelto por el servidor
     * @return valor asociado con la clave. "-- Acceso No Autorizado --" si no se puede resolver
     */	
	@Override
	public abstract String resolver(Usuario usuario, String clave);

	/**
	 * Funcion que comprueba si se pueden crear ciclos al anhadir como maestro 
	 * el servidor pasado por parametro.
	 * @param s El servidor a comprobar.
	 * @return True si se crean ciclos y False en caso de que no.
	 */
	private boolean check_relatives(Servidor s){
		for(Servidor serv = this.maestro; serv != null; serv = serv.obtenerMaestro())
			if(serv.equals(s))
				return true;
		return false;
	}
	
}
