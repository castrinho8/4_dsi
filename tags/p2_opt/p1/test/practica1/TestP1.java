package practica1;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Test;

public class TestP1 {

	@Test
	public void testBusquedaEstado(){
		Centralita c = new Centralita("password");
		
		Alarma a1 = new AlarmaTemperatura(5);
		Alarma a2 = new AlarmaTemperatura(9);
		Alarma a3 = new AlarmaMovimiento(3);
		Alarma a4 = new AlarmaMovimiento(4);
		
		Etiqueta e1 = new Etiqueta("lugar","Lugo");
		Etiqueta e2	= new Etiqueta("lugar","A Coruña");
		
		a1.agregarEtiqueta(e1);
		a3.agregarEtiqueta(e2);
	
		a2.probar();
		a3.probar();
		
		Especificacion esp = new EspecificacionEstado(true);
		
		assertTrue(esp.validar(a2));
		assertTrue(esp.validar(a3));
		assertFalse(esp.validar(a1));
		assertFalse(esp.validar(a4));
		
		assertFalse(c.autenticar("nopass"));
				
		assertTrue(c.autenticar("password"));
		
		c.agregarAlarma(a1);
		c.agregarAlarma(a2);
		c.agregarAlarma(a3);
		c.agregarAlarma(a4);
		
		Collection<Alarma> res =  c.buscar(esp);
		
		assertEquals(res.size(),2);
		assertTrue(res.contains(a2));
		assertTrue(res.contains(a3));
		
		c.reiniciar();
		
		res = c.buscar(esp);
		
		assertTrue(res.isEmpty());
	
	}
	
	@Test
	public void testZona() {

		Centralita c = new Centralita("Centralita1");
		
		Alarma a = new Zona();
		Alarma a1 = new AlarmaMovimiento(4);
		Alarma a2 = new AlarmaTemperatura(23);
		
		a.anhadirHijo(a1);
		a.anhadirHijo(a2);
		
		a.reiniciar();
		
		assertFalse(a.alerta());
		
		a1.probar();
		
		assertTrue(a.alerta());		
	}

}
