package practica1;

/**
 * Entidad que modela las etiquetas que se le pueden colocar a las alarmas.
 *
 */
public class Etiqueta {
	
	private String nombre;
	private String valor;
	
	/**
	 * Constructor de la clase.
	 * @param nombre El nombre que identifica la etiqueta.
	 * @param valor El valor que contiene la etiqueta.
	 */
	public Etiqueta(String nombre, String valor) {
		super();
		this.nombre = nombre;
		this.valor = valor;
	}
	
	/**
	 * Getter del nombre de la etiqueta.
	 * @return El nombre.
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Getter del valor de la etiqueta.
	 * @return El valor.
	 */
	public String getValor() {
		return valor;
	}
	
	/**
	 * Setter del valor de la etiqueta.
	 * @param s El valor a almacenar.
	 */
	public void setValor(String s){
		this.valor = s;
	}
	
}
