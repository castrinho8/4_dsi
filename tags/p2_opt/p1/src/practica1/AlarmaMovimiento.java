package practica1;

/**
 * Alarma basada en un sensor de movimiento
 * @see Alarma
 */
public class AlarmaMovimiento extends Alarma {
	static private int counter = 0;
	private int perMinimo;
	private int id;

	/**
	 * Constructor de la clase.
	 * @param perMinimo Periodo minimo de tiempo a partir del cual, con movimiento, se activa la alarma.
	 */
	public AlarmaMovimiento(int perMinimo) {
		super();
		this.perMinimo = perMinimo;
		this.id = counter++;
	}

	/**
	 * Getter del periodo de tiempo minimo.
	 * @return El periodo minimo de tiempo a partir del cual, con movimiento, se activa la alarma.
	 */
	public int getPerMinimo() {
		return perMinimo;
	}

	/**
	 * Setter del periodo de tiempo minimo.
	 * @param perMinimo El periodo minimo de tiempo a partir del cual, con movimiento, se activa la alarma
	 */
	public void setPerMinimo(int perMinimo) {
		this.perMinimo = perMinimo;
	}

	@Override
	/**
	 * Devuelve el identificador de la alarma.
	 * @return Identificador de la alarma.	  
	 */
	public String id_alarma() {
		return "AlarmaMovimiento" + this.id;
	}
	
}
