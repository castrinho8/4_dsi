package practica1;

/**
 * Alarma basada en la temperatura.
 * @see Alarma
 */
public class AlarmaTemperatura extends Alarma {
	
	private static int counter = 0;
	
	private int id;
	private int tempMaxima;

	/**
	 * Constructor de la clase.
	 * @param tempMaxima Temperatura a partir de la cual se activa la alarma.
	 */
	public AlarmaTemperatura(int tempMaxima) {
		super();
		this.tempMaxima = tempMaxima;
		this.id = counter++;
	}

	/**
	 * Getter de la temperatura maxima
	 * @return La temperatura a partir de la cual se activa la alarma.
	 */
	public int getTempMaxima() {
		return tempMaxima;
	}

	/**
	 * Setter de la temperatura maxima.
	 * @param tempMaxima La temperatura a partir de la cual se activa la alarma.
	 */
	public void setTempMaxima(int tempMaxima) {
		this.tempMaxima = tempMaxima;
	}

	@Override
	/**
	 * Devuelve el identificador de la alarma.
	 * @return Identificador de la alarma.
	 */
	public String id_alarma() {
		return "AlarmaTemperatura" + id;
	}
	
}
