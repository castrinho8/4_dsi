package practica1;

import java.util.ArrayList;

/**
 * 
 * Clase que modela zonas que pueden contener alarmas.
 *
 */
public class Zona extends Alarma {

	private ArrayList<Alarma> alarmas;
	private boolean validedCache;
	
	/**
	 * Constructor de la clase
	 */
	public Zona(){
		super();
		this.alarmas = new ArrayList<Alarma>();
		this.validedCache = false;
	}
	
	@Override
	/**
	 * Devuelve el identificador de la alarma.
	 * @return Identificador de la alarma.	  
	 */
	public String id_alarma() {
		String s = "";
		for(Alarma a : alarmas){
			s += a.id_alarma() + ",";
		}
		return "Zona de alarmas:" + s;
	}

	/**
	 * Anhade una alarma a la zona.
	 * @param a La alarma anhadir.
	 */
	public void anhadirHijo(Alarma a){
		if(a.obtenerPadre() != null){
			a.obtenerPadre().eliminarHijo(a);
		}
		a.cambiarPadreInterno(this);
		this.alarmas.add(a);
	}
	
	/**
	 * Elimina una alarma de la zona.
	 * @param a La alarma a eliminar.
	 */
	public void eliminarHijo(Alarma a){
		if(this.alarmas.contains(a)){
			this.alarmas.remove(a);
		}
	}
	
	/**
	 * Obtiene el hijo en la posición indicada.
	 * @param a La posición del hijo en la lista.
	 */
	public Alarma obtenerHijo(int a){
		return this.alarmas.get(a);
	}
	
	/**
	 * Invalida la caché y la del padre.
	 */
	public void invalidarCache(){
		this.validedCache = false;
		this.obtenerPadre().invalidarCache();
	}
	
	/**
	 * Comprueba el estado de la alarma.
	 * @return El estado de la alarma.
	 */
	public boolean alerta() {
		if(!this.validedCache){
			this.estado = false;
			for(Alarma a : alarmas)
				this.estado |= a.alerta();
		}
		return this.estado;
	}
	
	/**
	 * Reinicia el estado de las alarmas de la zona a false.
	 */
	public void reiniciar() {
		for(Alarma a : alarmas) a.reiniciar();
	}
	
	/**
	 * Activa las alarmas de la zona.
	 */
	public void probar() {
		for(Alarma a: alarmas) a.probar();
	}
	
	
	
	
}
