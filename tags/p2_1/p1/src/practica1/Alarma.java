package practica1;

import java.util.ArrayList;
import java.util.Collection;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Clase que modela una alarma.
 *
 */
public abstract class Alarma {
	
	protected boolean estado;
	private ArrayList<Etiqueta> etiquetas;
	private Alarma padre;
	
	/**
	 * Constructor de la clase.
	 */
	public Alarma() {
		this.estado = false;
		this.etiquetas = new ArrayList<Etiqueta>();
		this.padre = null;
	}
	
	/**
	 * Devuelve el estado de la alarma.
	 * @return El estado de la alarma.
	 */
	public boolean alerta() {
		return this.estado;
	}
	
	/**
	 * Devuelve el identificador de la alarma.
	 * @return Identificador de la alarma.	  
	 */
	public abstract String id_alarma();
	
	/**
	 * Reinicia el estado de la alarma a False e invalida la cache 
	 * correspondiente al padre.
	 */
	public void reiniciar() {
		if(this.obtenerPadre() != null) this.obtenerPadre().invalidarCache();
		this.estado = false;
	}
	
	/**
	 * Activa la alarma e invalida la cache correspondiente al padre.
	 */
	public void probar() {
		if(this.obtenerPadre() != null) this.obtenerPadre().invalidarCache();
		this.estado = true;
	}
	
	/**
	 * Anhade una etiqueta a la alarma.
	 * @param e La etiqueta a anhadir.
	 */
	public void agregarEtiqueta(Etiqueta e) {
		this.etiquetas.add(e);
	}

	/**
	 * Obtiene las etiquetas que tiene la alarma.
	 * @return Las etiquetas.
	 */
	public Collection<Etiqueta> getEtiquetas() {
		return this.etiquetas;
	}
	
	/**
	 * Obtiene el padre de la alarma.
	 * @return El padre.
	 */
	public Alarma obtenerPadre(){
		return this.padre;
	}
	
	/**
	 * Anhade una alarma hija.
	 * @param a La alarma a anhadir.
	 */
	public void anhadirHijo(Alarma a){
		throw new NotImplementedException();
	}
	
	/**
	 * Elimina una alarma hijo.
	 * @param a La alarma a eliminar.
	 */
	public void eliminarHijo(Alarma a){
		throw new NotImplementedException();
	}
	
	/**
	 * Establece la alarma pasada por parámetro como padre de la alarma
	 * que llama al método.
	 * @param a La alarma que va a ser padre.
	 */
	public void EstablecerPadre(Alarma a){
		if(this.padre != null){
			this.padre.eliminarHijo(this);
		}
		a.anhadirHijo(this);
	}
	
	/**
	 * Setter del padre.
	 * @param a El nuevo padre.
	 */
	protected void cambiarPadreInterno(Alarma a){
		padre = a;
	}
	
	/**
	 * Devuelve el hijo en la posición indicada dentro de la lista.
	 * @param a La posición del hijo.
	 * @return La alarma hija.
	 */
	public Alarma obtenerHijo(int a){
		throw new NotImplementedException();
	}
	
	/**
	 * Invalida la caché.
	 */
	public void invalidarCache(){
		throw new NotImplementedException();
	}
	
}
