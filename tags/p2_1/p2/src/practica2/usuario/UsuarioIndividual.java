package practica2.usuario;

public class UsuarioIndividual implements Usuario {

    public UsuarioIndividual(String nombre) {
        _nombre = nombre;
        _id = obtenerSiguienteID();
    }

    private int obtenerSiguienteID() {
        return _ultimoID++;
    }


    public String obtenerNombre() {
        return _nombre;
    }


    public String toString() {
        return obtenerNombre();
    }


    public int obtenerID() {
        return _id;
    }


    public boolean autorizar(Usuario otroUsuario) {
	return (otroUsuario.obtenerID() == obtenerID());
    }

    //------------------------------------------------------------

    private String _nombre;
    private int _id;

    //------------------------------------------------------------
    //
    // atributo de clase, lo usamos para mantener un contador único

    private static int _ultimoID = 0;
}
