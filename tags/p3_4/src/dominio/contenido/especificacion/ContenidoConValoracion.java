package dominio.contenido.especificacion;

import dominio.contenido.Contenido;

public class ContenidoConValoracion implements EspecificacionContenido {

	private int valoracion;
	
	/**
	 * Contructor de la clase.
	 * @param valoracion La valoracion a partir de la cual los contenidos son validos.
	 */
	public ContenidoConValoracion(int valoracion){
		this.valoracion = valoracion;
	}

	/**
	 * Método que indica si el contenido pasado por parametro esta dentro del filtro o no.
	 * @param contenido Instancia del contenido que se quiere comprobar.
	 * @return El booelano <em>true</em> si la valoración del contenido pasado por parametro es mayor o igual a la del filtro. Devuelve <em>false</em> en caso contrario.
	 */
	@Override
	public boolean valido(Contenido contenido) {
		return contenido != null && contenido.obtenerValoracion() >= this.valoracion;
	}

	/**
	 * Método que aporta una descripción del filtro.
	 * @return Un String con una descripción del filtro.
	 */
	@Override
	public String descripcion() {
		return "Filtro para contenidos con valoración mayor a " + this.valoracion;
	}

	/**
	 * Método toString de la clase.
	 */
	@Override
	public String toString() {
		return this.descripcion();
	}
	
	

}
