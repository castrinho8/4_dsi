package dominio.contenido.especificacion;

import dominio.contenido.Contenido;
/**
 * Clase que implementa un AND de Especificaciones.
 */
public class EspecificacionAND implements EspecificacionContenido {

	private EspecificacionContenido[] especificaciones;

	/**
	 * Constructor de la clase.
	 * @param especificaciones Array de especificaciones de las que se hace AND.
	 */
	public EspecificacionAND(EspecificacionContenido especificaciones[]) {
		this.especificaciones = especificaciones;
	}

	/**
	 * Metodo que aporta una descripción del filtro.
	 * @return Un String con una descripcion del filtro.
	 */
	@Override
	public String descripcion() {
		String s = "";
		for(EspecificacionContenido e : especificaciones){
			s = e.descripcion() + (s.equals("") ? "" : " AND ") + s;
		}
		return s;
	}

	/**
	 * Método que indica si el contenido pasado por parametro esta dentro del filtro o no.
	 * @param contenido Instancia del contenido que se quiere comprobar.
	 * @return El booelano <em>true</em> si el contenido pasado por parametro es valido para todas las especificaciones de las que se hace AND. Devuelve <em>false</em> en caso contrario.
	 */
	@Override
	public boolean valido(Contenido contenido) {
		for(EspecificacionContenido e : especificaciones){
			if(!e.valido(contenido))
				return false;
		}
		return true;
	}

	/**
	 * Método toString de la clase.
	 * @return un String con la descripción del filtro.
	 */
	@Override
	public String toString() {
		return this.descripcion();
	}
	

}
