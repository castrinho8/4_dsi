package dominio.contenido.elemento;

import dominio.Buzon;
import dominio.usuario.Usuario;

/**
 * Clase que modela un elemento pendiente de borrado.
 *
 */
public class ElementoPendienteBorrado extends ElementoPendiente {
	
	/**
	 * Constructor de la clase ElementoPendienteBorrado
	 * @param elemento El elemento pendiente de borrado.
	 * @param editor El editor que tiene permisos sobre el elemento.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos.
	 */
	public ElementoPendienteBorrado(Elemento elemento, Usuario editor)
	throws OperacionInvalida {
		super(elemento, editor);
	}
	
	/**
	 * Método que devuelve el codigo que indica la situación en la que se encuentra
	 * el elemento.
	 * @param elemento El elemento.
	 * @return El codigo de situacion, en este caso "PB"
	 */
	public String codigoSituacion(Elemento elemento) {
		return "PB";
	}
	
	/**
	 * Método que realiza la aprobación de un elemento pendiente de aprobación.
	 * @param elemento El elemento a aprobar.
	 */
	protected void aprobarTransicion(Elemento elemento)
	throws OperacionInvalida {
		elemento.obtenerPadre().eliminar(elemento);
		elemento.establecerSituacion(new ElementoBorrado());
	}
	
	/**
	 * Método que agrega una nueva revisión al buzon.
	 * @param revisor El revisor a agregar.
	 * @param elemento El elemento a agregar.
	 */
	protected void agregarMensaje(Usuario revisor, Elemento elemento) {
		Buzon.instancia().agregarBorrado(revisor, elemento);
	}
	
	/**
	 * Método que elimina una revisión del buzón.
	 * @param revisor El revisor a agregar.
	 * @param elemento El elemento a agregar.
	 */
	protected void eliminarMensaje(Usuario revisor, Elemento elemento) {
		Buzon.instancia().eliminarBorrado(revisor, elemento);
	}
	
	/**
	 * Método que devuelve la descripción de la situación.
	 * @return La descripción de la situación, en este caso "pendiente borrado"
	 */
	protected String descripcion() {
		return "pendiente borrado";
	}
	
}
