package dominio.contenido.elemento;

/**
 * Clase que modela un elemento que se encuentra borrado.
 *
 */
public class ElementoBorrado extends SituacionElemento {
	
	/**
	 * Método que devuelve el codigo que indica la situación en la que se encuentra
	 * el elemento.
	 * @param elemento El elemento.
	 * @return El codigo de situacion, en este caso "B"
	 */
	public String codigoSituacion(Elemento elemento) {
		return "B";
	}
	
	/**
	 * Método que devuelve la descripción de la situación.
	 * @return La descripción de la situación, en este caso "borrado"
	 */
	protected String descripcion() {
		return "borrado";
	}
	
}
