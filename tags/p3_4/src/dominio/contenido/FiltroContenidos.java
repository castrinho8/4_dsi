package dominio.contenido;

import dominio.contenido.especificacion.EspecificacionContenido;

/**
 * Lista de contenidos que se implementa como un filtro de otra lista.
 */
public class FiltroContenidos implements ListadoContenidos {
	
	private ListadoContenidos _listado;
	private EspecificacionContenido _filtro;
	
	/**
	 * Contructor de la clase.
	 * @param listado Lista de partida.
	 * @param filtro Filtro de la lista.
	 */
	public FiltroContenidos(ListadoContenidos listado, EspecificacionContenido filtro) {
		_listado = listado;
		_filtro = filtro;
	}
	/**
	 * Método que devuelve el siguiente elemento de la lista.
	 * @return El siguiente Contenido de la lista.
	 */
	public Contenido siguiente() {
		Contenido contenido;
		while ((contenido = _listado.siguiente()) != null
				&& !_filtro.valido(contenido));
		return contenido;
	}
}
