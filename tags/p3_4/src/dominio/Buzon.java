package dominio;

import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

import util.Interesante;
import dominio.contenido.elemento.Elemento;
import dominio.contenido.elemento.ListadoElementos;
import dominio.contenido.elemento.ListadoElementosColeccion;
import dominio.contenido.elemento.ListadoElementosNulo;
import dominio.usuario.Usuario;

/**
 * Clase que modela un buzon en donde se reciven los elementos que se han revisado y/o borrado.
 * Esta clase implementa o patrón instancia única.
 */
public class Buzon extends Interesante {
	
	private HashMap<Usuario, Collection<Elemento>> _revisiones;
	private HashMap<Usuario, Collection<Elemento>> _borrados;
	private static Buzon _instancia = new Buzon();
	
	/**
	 * Constructor privado de la clase.
	 */
	private Buzon() {
		_revisiones = new HashMap<Usuario, Collection<Elemento>>();
		_borrados = new HashMap<Usuario, Collection<Elemento>>();
	}
	
	/**
	 * Método que devuelve una instancia de la clase.
	 * @return La intancia unica de la clase.
	 */
	public static Buzon instancia() {
		return _instancia;
	}
	
	/**
	 * Método que agrega una nueva revisión al buzón.
	 * @param usuario Usuario que hizo la revisión.
	 * @param elemento Elemento revisado.
	 */
	public void agregarRevision(Usuario usuario, Elemento elemento) {
		agregar(_revisiones, usuario, elemento);
	}
	
	/**
	 * Metodo que permite registrar un borrado de un elemento.
	 * @param usuario Usuario que efectuó el borrado.
	 * @param elemento Elemento borrado.
	 */
	public void agregarBorrado(Usuario usuario, Elemento elemento) {
		agregar(_borrados, usuario, elemento);
	}
	
	/**
	 * Método que permite eliminar una revisión previamente guardada.
	 * @param usuario Usuario de la revisión a eliminar.
	 * @param elemento Elemento que de la revisión a eliminar.
	 */
	public void eliminarRevision(Usuario usuario, Elemento elemento) {
		eliminar(_revisiones, usuario, elemento);
	}
	
	/**
	 * Método que permite eliminar un borrado registrado previamente.
	 * @param usuario Usuario del borrado.
	 * @param elemento Elemento que se borro.
	 */
	public void eliminarBorrado(Usuario usuario, Elemento elemento) {
		eliminar(_borrados, usuario, elemento);
	}
	
	/**
	 * Método devuelve una lista de los elementos revisados por un determinado usuario.
	 * @param usuario Usuario cuyas revisiones se quieren listar.
	 * @return Lista de elementos.
	 */
	public ListadoElementos obtenerRevisiones(Usuario usuario) {
		return obtener(_revisiones, usuario);
	}
	
	/**
	 * Método que devuelve una lista de los elementos borrados por un determinado usuario.
	 * @param usuario Usuario cuyos borrados se quieren listar.
	 * @return Lista de elementos borrados.
	 */
	public ListadoElementos obtenerBorrados(Usuario usuario) {
		return obtener(_borrados, usuario);
	}
	
	/**
	 * Método privado que agrega un elemento a una lista.
	 * @param tabla Lista a la que agregar el elemento.
	 * @param usuario Usuario que modificó el elemento.
	 * @param elemento Elemento a agregar.
	 */
	private void agregar(HashMap<Usuario, Collection<Elemento>> tabla, Usuario usuario, Elemento elemento) {
		Collection<Elemento> elementos;
		if (tabla.containsKey(usuario)) {
			elementos = tabla.get(usuario);
			elementos.remove(elemento);
		} else {
			elementos = new Vector<Elemento>();
		}
		elementos.add(elemento);
		tabla.put(usuario, elementos);
		avisarInteresados();
	}
	
	/**
	 * Método privado que elimina un elemento de una lista.
	 * @param tabla Lista de la que borrar el elemento.
	 * @param usuario Usuario que modifico el elemento a borrar.
	 * @param elemento Elemento a borrar.
	 */
	private void eliminar(HashMap<Usuario, Collection<Elemento>> tabla, Usuario usuario, Elemento elemento) {
		if (tabla.containsKey(usuario)) {
			Collection<Elemento> elementos = tabla.get(usuario);
			elementos.remove(elemento);
		}
		avisarInteresados();
	}
	
	/**
	 * Metodo que devuelve una lista de elementos modificados por un usuario.
	 * @param tabla Lista en la que se buscan los elementos.
	 * @param usuario Usuario que modifico los elementos.
	 * @return lista de elementos modificados por el usuario dado dentro de la lista.
	 */
	private ListadoElementos obtener(
			HashMap<Usuario, Collection<Elemento>> tabla, Usuario usuario) {
		if (tabla.containsKey(usuario)) {
			return new ListadoElementosColeccion(tabla.get(usuario));
		} else {
			return new ListadoElementosNulo();
		}
	}
	
}
