package util;

/**
 * Interfaz de los interesados
 *
 */
public interface Interesado {

	/**
	 * Método que avisa a los interesados.
	 */
	public void actualizar();
	
}
