package dominio.contenido.elemento;

import util.Interesado;
import dominio.contenido.Contenido;
import dominio.usuario.Usuario;

/**
 * Clase que modela un elemento genérico del sistema.
 * Este Elemento a su vez se puede especializar en un Parrafo o en un ElementoCompuesto
 * @see Parrafo
 * @see ElementoCompuesto
 *
 */
public abstract class Elemento extends Contenido implements Interesado {
	
	/**
	 * Constructor de la clase Elemento.
	 * Inicializa el texto del elemento con el texto introducido por parámetro, le asigna
	 * el estado de ElementoVisible y el padre se inicializa a null.
	 * @param texto El texto que contiene el elemento.
	 */
	public Elemento(String texto) {
		_texto = texto;
		_padre = null;
		_situacion = new ElementoVisible();
	}
	
	/**
	 * Método que agrega el elemento pasado por parámetro al elemento que 
	 * llama al método.
	 * Implementación por defecto del método agregar que devuelve una excepción.
	 * @param elemento El elemento a agregar.
	 * @throws OperacionInvalida En caso de que este elemento no puede alojar a otros elementos.
	 */
	public void agregar(Elemento elemento) throws OperacionInvalida {
		throw new OperacionInvalida("Este elemento no puede alojar otros elementos");
	}
	
	/**
	 * Método que elimna el elemento pasado por parámetro del elemento que
	 * llama al método.
	 * Implementación por defecto del método eliminar que devuelve una excepción.
	 * @param elemento El elemento a eliminar.
	 * @throws OperacionInvalida En caso de que este elemento no aloje a otros elementos.
	 */
	public void eliminar(Elemento elemento) throws OperacionInvalida {
		throw new OperacionInvalida("Este elemento no aloja otros elementos");
	}
	
	/**
	 * Método que obtiene el elemento que se encuentra 
	 * en la posición indicada por parámetro.
	 * @param posicion La posición en la que se encuentra el elemento.
	 * @return El elemento a recuperar.
	 * @throws OperacionInvalida En caso de que el elemento no aloje a otros elementos.
	 */
	public Elemento obtenerElemento(int posicion) throws OperacionInvalida {
		throw new OperacionInvalida("Este elemento no aloja otros elementos");
	}
	
	/**
	 * Getter que obtiene el propietario del elemento.
	 * @return El usuario propietario.
	 */
	public Usuario obtenerPropietario() {
		if (obtenerPadre() != null) {
			return obtenerPadre().obtenerPropietario();
		} else {
			return super.obtenerPropietario();
		}
	}
	
	/**
	 * Setter del propietario del elemento.
	 * @param propietario El nuevo usuario propietario.
	 */
	public void establecerPropietario(Usuario propietario) {
		if (obtenerPadre() != null) {
			obtenerPadre().establecerPropietario(propietario);
		} else {
			super.establecerPropietario(propietario);
		}
	}
	
	/**
	 * Getter del título.
	 * @return El string que contiene el título.
	 */
	public String obtenerTitulo() {
		if (obtenerPadre() != null) {
			return obtenerPadre().obtenerTitulo();
		} else {
			return super.obtenerTitulo();
		}
	}
	
	/**
	 * Setter del titulo
	 * @param titulo El nuevo título.
	 */
	public void establecerTitulo(String titulo) {
		if (obtenerPadre() != null) {
			obtenerPadre().establecerTitulo(titulo);
		} else {
			super.establecerTitulo(titulo);
		}
	}
	
	/**
	 * Método que obtiene el primer elemento de todos.
	 * El padre de una jerarquía.
	 * @return El primer elemento de una jerarquía.
	 */
	public Elemento obtenerRaiz() {
		if (obtenerPadre() != null) {
			return obtenerPadre().obtenerRaiz();
		} else {
			return this;
		}
	}
	
	/**
	 * Getter del texto de un elemento.
	 * @return El texto.
	 */
	public String obtenerTexto() {
		return _texto;
	}
	
	/**
	 * Setter del texto de un elemento.
	 * Avisa a los elemento que estén interesados en este objeto de 
	 * que se ha realizado los cambios.
	 * @param texto El nuevo texto del elemento.
	 */
	public void establecerTexto(String texto) {
		_texto = texto;
		avisarInteresados();
	}
	
	/**
	 * Método que comprueba si un usuario ha valorado o no el contenido.
	 * Implementación por defecto que devuelve False en cualquier caso. 
	 * @param usuario Usuario que se desea comprobar.
	 * @return Un booleano que indica si el usuario ha valorado o no el contenido.
	 */
	public boolean valorado(Usuario usuario) {
		return false;
	}
	
	/**
	 * Método que devuelve la valoración del contenido.
	 * Implementación por defecto que devuelve 0 en cualquier caso.
	 * @return un entero que especifica la valoracion del contenido.
	 */
	public int obtenerValoracion() {
		return 0;
	}
	
	/**
	 * Método que cambia la situación del elemento a EnEdicion
	 * Delega el metodo en la situación del elemento.
	 * en caso de que el usuario esté autorizado.
	 * @param editor El usuario.
	 * @throws OperacionInvalida En caso de que el usuario no disponga de permisos.
	 */
	public void iniciarEdicion(Usuario editor) throws OperacionInvalida {
		_situacion.iniciarEdicion(this, editor);
	}
	
	/**
	 * Método que cancela la edición del propio elemento y coloca su 
	 * situación en ElementoVisible en caso de que el usuario esté autorizado.
	 * Delega el metodo en la situación del elemento.
	 * @param editor El usuario 
	 * @throws OperacionInvalida En caso de que el usuario no disponga de permisos.
	 */
	public void cancelarEdicion(Usuario editor) throws OperacionInvalida {
		_situacion.cancelarEdicion(this, editor);
	}
	
	/**
	 * Método que devuelve la revisión de un elemento pendiente de aprobación.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha acción.
	 * Delega el metodo en la situación del elemento.
	 * @param usuario El usuario que obtiene la revisión.
	 * @return El String con la revisión.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos.
	 */
	public String obtenerRevision(Usuario usuario) throws OperacionInvalida {
		return _situacion.obtenerRevision(this, usuario);
	}
	
	/**
	 * Método que realiza una proposición para que el elemento sea revisado.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha proposición.
	 * Delega el metodo en la situación del elemento.
	 * @param editor El editor que realiza la proposición.
	 * @param revision Revisión a realizar.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la revisión.
	 */
	public void proponerRevision(Usuario editor, String revision)
	throws OperacionInvalida {
		_situacion.proponerRevision(this, editor, revision);
	}
	
	/**
	 * Método que realiza una proposición para que el elemento pasado por parámetro, sea revisado.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha proposición.
	 * Delega el metodo en la situación del elemento.
	 * @param editor El editor que realiza la proposición.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la revisión.
	 */
	public void proponerBorrado(Usuario editor) throws OperacionInvalida {
		_situacion.proponerBorrado(this, editor);
	}
	
	/**
	 * Método que realiza la aprobación de una modificacion de un elemento.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha acción.
	 * Delega el metodo en la situación del elemento.
	 * @param usuario El usuario que realiza la aprobación.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la acción.
	 */
	public void aprobar(Usuario usuario) throws OperacionInvalida {
		_situacion.aprobar(this, usuario);
	}
	
	/**
	 * Método que realiza la denegación de una modificacion de un elemento.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha acción.
	 * Delega el metodo en la situación del elemento.
	 * @param usuario El usuario que realiza la denegación.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la acción.
	 */
	public void denegar(Usuario usuario) throws OperacionInvalida {
		_situacion.denegar(this, usuario);
	}
	
	/**
	 * Método que devuelve el codigo que indica la situación en la que se encuentra
	 * el elemento.
	 * Delega el metodo en la situación del elemento.
	 * @return El codigo de situacion, en este caso "-"
	 */
	public String codigoSituacion() {
		return _situacion.codigoSituacion(this);
	}
	
	/**
	 * Método que obtiene los elementos de los que se compone este elemento. 
	 * Implementación por defecto que devuelve una lista de elementos nula.
	 * @return El listado de elementos.
	 */
	public ListadoElementos obtenerElementos() {
		return new ListadoElementosNulo();
	}
	
	/**
	 * Getter del padre.
	 * @return El usuario padre.
	 */
	public Elemento obtenerPadre() {
		return _padre;
	}
	
	/**
	 * Método que copia el estado de un contenido en este objeto.
	 * @return La copia del contenido.
	 */
	public abstract Contenido copiar();
	
	/**
	 * Método que comprueba la autorización de un usuario.
	 * @param usuario El usuario a validar
	 * @return True en caso de estar autorizado y False en caso contrario.
	 */
	public boolean autorizado(Usuario usuario) {
		return ((obtenerPropietario() != null) && (obtenerPropietario().contiene(usuario)));
	}
	
	/**
	 * Método que realiza la actualización avisando a los interesados
	 */
	public void actualizar() {
		avisarInteresados();
	}
	
	/**
	 * Setter de la situacion que tambien avisa a los interesados.
	 * @param situacion La nueva situacion.
	 */
	void establecerSituacion(SituacionElemento situacion) {
		_situacion = situacion;
		avisarInteresados();
	}
	
	/**
	 * Setter del padre.
	 * @param padre El nuevo padre.
	 */
	protected void establecerPadre(Elemento padre) {
		_padre = padre;
	}
	
	/**
	 * Método que copia el elemento invocador en el elemento que se le 
	 * pasa por parámetro, setteando la situacion a ElementoVisible.
	 * @param copia El elemento en el que copiar los elementos.
	 */
	protected void copiarEstado(Elemento copia) {
		copia._texto = _texto;
		copia._padre = _padre;
		copia._situacion = new ElementoVisible();
	}
	
	private SituacionElemento _situacion;
	
	private Elemento _padre;
	
	private String _texto;
	
}
