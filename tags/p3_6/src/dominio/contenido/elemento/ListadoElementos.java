package dominio.contenido.elemento;

/**
 * Interfaz del Listado de Elementos
 *
 */
public interface ListadoElementos {
	
	/**
	 * Función que devuelve el siguiente Elemento de la lista.
	 * @return El elemento siguiente.
	 */
	public Elemento siguiente();
	
}
