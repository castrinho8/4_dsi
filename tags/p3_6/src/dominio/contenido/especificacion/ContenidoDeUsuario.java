package dominio.contenido.especificacion;

import dominio.contenido.Contenido;
import dominio.usuario.Usuario;

/**
 * Clase que implementa un filtro de contenidos por su propietario.
 * @see EspecificacionContenido
 */
public class ContenidoDeUsuario implements EspecificacionContenido {

	private Usuario usuario;

	/**
	 * Contructor de la clase ContenidoDeUsuario.
	 * @param u El usuario por el que se filtran los contenidos.
	 */
	public ContenidoDeUsuario(Usuario u){
		this.usuario = u;
	}
	

	/**
	 * Método que indica si el contenido pasado por parametro esta dentro del filtro o no.
	 * @param contenido Instancia del contenido que se quiere comprobar.
	 * @return El booleano <em>true</em> si el propietario del contenido pasado por parametro es el mismo que el del filtro. Devuelve <em>false</em> en caso contrario.
	 */
	@Override
	public boolean valido(Contenido contenido) {
		return contenido != null && contenido.obtenerPropietario().equals(usuario);
	}

	/**
	 * Metodo que aporta una descripción del filtro.
	 * @return Un String con una descripcion del filtro.
	 */
	@Override
	public String descripcion() {
		return "Contenidos del usuario \"" + usuario.toString() + "\"";
	}

	/**
	 * Método toString de la clase.
	 * @return un String con la descripción del filtro.
	 */
	@Override
	public String toString() {
		return this.descripcion();
	}
	
}
