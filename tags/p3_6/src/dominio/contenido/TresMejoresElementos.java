package dominio.contenido;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import dominio.GestorContenidos;
import dominio.contenido.especificacion.CualquierContenido;
import util.Interesado;

/**
 * Observador que guarda una lista de los tres elementos mejor valorados.
 */
public class TresMejoresElementos implements Interesado {

	private static TresMejoresElementos instance;
	private ArrayList<Contenido> tresmejores;
	
	/**
	 * Contructor de la clase.
	 * Inicializa la lista de los tres elementos mejores.
	 */
	public TresMejoresElementos(){
		this.tresmejores =  new ArrayList<Contenido>(3);
	}
	
	/**
	 * Actualiza la lista de los tres elementos mejores.
	 * Este método es llamado por cualquier elemento observado.
	 */
	@Override
	public void actualizar() {
		
		//Obtiene la lista de elementos
		Contenido e;
		ListadoContenidos l = GestorContenidos.instancia().obtenerContenidos(new CualquierContenido());
		if (l == null) return;
		ArrayList<Contenido> cont = new ArrayList<Contenido>();
		while((e = l.siguiente()) != null)
			cont.add(e);
		
		//Ordena la lista de elementos.
		Collections.sort(cont, new Comparator<Contenido>(){
			@Override
			public int compare(Contenido arg0, Contenido arg1) {
				return new Integer(arg1.obtenerValoracion()).compareTo(arg0.obtenerValoracion());
			}
		});
		
		//Inserta en la lista los 3 mejor valorados.
		tresmejores.clear();
		try{
			tresmejores.add(cont.get(0));
			tresmejores.add(cont.get(1));
			tresmejores.add(cont.get(2));
		}catch(IndexOutOfBoundsException ee){}
		
		System.out.println("Ahora los tres elementos mas valorados son:");
		for(Contenido c : tresmejores){
			System.out.println(c);
		}
	}
	/**
	 * Método que devuelve una instancia de la clase.
	 * En caso de que no haya una instancia creada, se crea.
	 * @return La intancia unica de la clase.
	 */
	public static TresMejoresElementos instancia() {
		if(instance == null)
			instance = new TresMejoresElementos();
		return instance;
	}
}
