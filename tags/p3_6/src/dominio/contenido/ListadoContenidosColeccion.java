package dominio.contenido;

import java.util.Collection;
import java.util.Vector;

/**
 * Implementación de una lista de contenidos.
 */
public class ListadoContenidosColeccion implements ListadoContenidos {
	
	private Vector<Contenido> _contenidos;
	private int _posicion;
	
	/**
	 * Contructor de la clase ListadoContenidosColeccion
	 * Inicializa un vector con los contenidos pasados por parámetro.
	 * @param contenidos Colección de contenidos.
	 */
	public ListadoContenidosColeccion(Collection<Contenido> contenidos) {
		_contenidos = new Vector<Contenido>(contenidos);
		_posicion = 0;
	}

	/**
	 * Método que devuelve el siguiente elemento de la lista.
	 * 
	 * @return El siguiente Contenido de la lista o null en el caso de no existir.
	 */
	public Contenido siguiente() {
		if (_posicion < _contenidos.size()) {
			return _contenidos.elementAt(_posicion++);
		} else {
			return null;
		}
	}
}
