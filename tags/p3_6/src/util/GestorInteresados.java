/**
 * 
 */
package util;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Clase que modela un Gestor de Interesados que almacena en su interior una lista
 *  de elementos interesados y los objetos que los quieren observar.
 */
public class GestorInteresados {

	private static GestorInteresados instance;
	private HashMap<Interesante, ArrayList<Interesado>> interesados;

	/**
	 * Constructor privado del Gestor Interesados.
	 * Inicializa un hashmap vacio en el que se guarda, para cada interesante, los objetos en los que 
	 * está interesado.
	 */
	private GestorInteresados(){
		this.interesados = new HashMap<Interesante,ArrayList<Interesado>>();
	}
	
	/**
	 * Devuelve una instancia de GestorInteresados.
	 * @return Una instancia unica de Gestor de Interesados.
	 */
	static GestorInteresados instance(){
		if (instance == null){
			instance = new GestorInteresados();
		}
		return instance;
	}
	
	/**
	 * Agrega un elemento interesado al gestor.
	 * @param i Elemento que el interesado quiere observar.
	 * @param o Elemento a añadir.
	 */
	public void agregarInteresado(Interesante i, Interesado o) {
		if(!this.interesados.containsKey(i))
			this.interesados.put(i, new ArrayList<Interesado>());
		
		this.interesados.get(i).add(o);
	}
	
	/**
	 * Elimina un interesado del gestor de interesados.
	 * Si el interesado no existe no hace nada.
	 * @param i Elemento que el interesado quiere observar.
	 * @param o Elemento a eliminar.
	 */
	public void eliminarInteresado(Interesante i, Interesado o) {
		try{
			this.interesados.get(i).remove(o);	
		}catch(NullPointerException e ){}
	}
	
	/**
	 * Metodo que avisa a los elementos interesados en un determinado objeto.
	 * Si no hay ningun elemento interesado no se hace nada.
	 * @param i Elemento en el que estan interesados los objetos a avisar.
	 */
	public void avisarInteresados(Interesante i) {
		ArrayList<Interesado> lista = this.interesados.get(i);
		if (lista == null)
			return;
		for(Interesado i2 : lista)
			i2.actualizar();
	}
	
}
