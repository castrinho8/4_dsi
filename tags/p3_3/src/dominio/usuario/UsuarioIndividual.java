package dominio.usuario;

import java.util.Vector;

/**
 * Clase que modela el comportamiento de un usuario individual.
 *
 */
public class UsuarioIndividual extends Usuario {
	
	/**
	 * Constructor de la clase UsuarioIndividual
	 * @param nombre
	 */
	public UsuarioIndividual(String nombre) {
		super(nombre);
	}
	
	/**
	 * Función que comprueba si el usuario indicado por parámetro es el mismo 
	 * que el usuario que llama a la función.
	 * @param usuario El usuario a comprobar.
	 * @return True en caso de que se encuentre y False en caso contrario.
	 */
	public boolean contiene(Usuario usuario) {
		return equals(usuario);
	}
	
	/**
	 * Devuelve un vector con este usuario individual como único miembro.
	 * @return El vector con este usuario.
	 */
	protected Vector<Usuario> obtenerListadoMiembros() {
		Vector<Usuario> resultado = new Vector<Usuario>();
		resultado.addElement(this);
		return resultado;
	}
	
}
