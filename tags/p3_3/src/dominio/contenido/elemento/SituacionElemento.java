package dominio.contenido.elemento;

import dominio.usuario.Usuario;

/**
 * Clase abstracta que modela los estados/situaciones en los que se encuentra el elemento.
 *
 */
public abstract class SituacionElemento {
	
	/**
	 * Método que inicia el estado de edición en caso de estar autorizado
	 * el usuario indicado por parámetro.
	 * @param elemento El elemento a editar.
	 * @param editor El usuario que inicia la edición.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la edición.
	 */
	public void iniciarEdicion(Elemento elemento, Usuario editor)
	throws OperacionInvalida {
		invalida("iniciar edición");
	}
	
	/**
	 * Método que cancela la edición del elemento introducido como parámetro.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha cancelación.
	 * @param elemento El elemento a cancelar su edición.
	 * @param editor El editor que realiza la cancelación.
	 * @throws OperacionInvalida  En caso de que no se disponga de permisos para realizar la cancelación.
	 */
	public void cancelarEdicion(Elemento elemento, Usuario editor)
	throws OperacionInvalida {
		invalida("cancelar edición");
	}
	
	/**
	 * Getter de la revisión de un Elemento Pendiente de Aprobación.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha acción.
	 * @param elemento El elemento del que obtener la revisión.
	 * @param usuario El usuario que obtiene la revisión.
	 * @return El String con la revisión.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos.
	 */
	public String obtenerRevision(Elemento elemento, Usuario usuario)
	throws OperacionInvalida {
		invalida("obtener revisión");
		return null;
	}
	
	/**
	 * Método que realiza una proposición para que el elemento pasado por parámetro, sea revisado.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha proposición.
	 * @param elemento El elemento a proponer para revisión.
	 * @param editor El editor que realiza la proposición.
	 * @param revision Revisión a realizar.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la revisión.
	 */
	public void proponerRevision(Elemento elemento, Usuario editor,	String revision) 
	throws OperacionInvalida {
		invalida("proponer revisión");
	}
	
	/**
	 * Método que propone el elemento pasado por parámetro para su eliminación.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha proposición.
	 * @param elemento El elemento a eliminar.
	 * @param editor El usuario que propone el borrado.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la edición.
	 */
	public void proponerBorrado(Elemento elemento, Usuario editor)
	throws OperacionInvalida {
		invalida("proponer borrado");
	}
	
	/**
	 * Método que realiza la aprobación de la modificación de un elemento.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha acción.
	 * @param elemento El elemento. 
	 * @param usuario El usuario que realiza la aprobación.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la acción.
	 */
	public void aprobar(Elemento elemento, Usuario usuario)
	throws OperacionInvalida {
		invalida("aprobar");
	}
	
	/**
	 * Método que realiza la denegación de la modificación de un elemento.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha acción.
	 * @param elemento El elemento. 
	 * @param usuario El usuario que realiza la denegación.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la acción.
	 */
	public void denegar(Elemento elemento, Usuario usuario)
	throws OperacionInvalida {
		invalida("denegar");
	}
	
	/**
	 * Método que devuelve el codigo que indica la situación en la que se encuentra
	 * el elemento.
	 * @param elemento El elemento.
	 * @return El codigo de situacion.
	 */
	public abstract String codigoSituacion(Elemento elemento);
	
	/**
	 * Método que devuelve la descripción de la situación.
	 * @return La descripción de la situación.
	 */
	protected abstract String descripcion();
	
	/**
	 * Método que lanza una excepción de operación inválida cuando se trata de
	 * realizar una operación que no se puede realizar debido al estado en el que se 
	 * encuentra el elemento.
	 * @param operacion La operación que motiva la excepción.
	 * @throws OperacionInvalida En caso de que la operación realizada no se pueda realizar.
	 */
	private void invalida(String operacion) throws OperacionInvalida {
		throw new OperacionInvalida(operacion, descripcion());
	}
	
}
