package dominio.contenido.elemento;

/**
 * Clase que modela una excepción de operación invalida.
 *
 */
@SuppressWarnings("serial")
public class OperacionInvalida extends Exception {
	
	/**
	 * Constructor de la clase OperacionInvalida.
	 * @param descripcion La descripción del error.
	 */
	public OperacionInvalida(String descripcion) {
		super(descripcion);
	}
	
	/**
	 * Constructor de la clase OperacionInvalida.
	 * @param operacion La operación que ha lanzado la excepción.
	 * @param descripcionSituacion La descripción de la situación del elemento.
	 */
	public OperacionInvalida(String operacion, String descripcionSituacion) {
		this("La operación <" + operacion + "> "
				+ "no puede realizarse cuando el elemento está "
				+ descripcionSituacion);
	}
	
}
