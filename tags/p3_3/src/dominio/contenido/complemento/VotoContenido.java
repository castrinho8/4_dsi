package dominio.contenido.complemento;

import dominio.contenido.Contenido;
import dominio.usuario.Usuario;

/**
 * Complemento del contenido que representa un voto de este.
 */
public class VotoContenido extends ComplementoContenido {
	
	private Usuario _votante;
	
	/**
	 * Contructor de la clase.
	 * @param contenido Contenido al que pertenece el voto.
	 * @param votante Usuario que lo realizó.
	 */
	public VotoContenido(Contenido contenido, Usuario votante) {
		super(contenido);
		_votante = votante;
	}
	
	/**
	 * Método que comprueba si un usuario ha valorado o no el contenido.
	 *  Si el usuario pasado por parametro es el mismo que el almacenado en esta 
	 *  	clase, devuelve true; en caso contrario delega el metodo de la clase padre.
	 * @param votante Usuario que se desea comprobar.
	 * @return Un booleano que indica si el usuario ha valorado o no el contenido.
	 */
	public boolean valorado(Usuario votante) {
		if (_votante.equals(votante)) {
			return true;
		}
		return super.valorado(votante);
	}
	
	/**
	 * Método que devuelve la valoración del contenido.
	 * Obtiene la valoración de la clase padre y le suma 1.
	 * @return un entero que especifica la valoracion del contenido.
	 */
	public int obtenerValoracion() {
		return super.obtenerValoracion() + 1;
	}
	
	/**
	 * Método que copia el estado de un contenido en otro objeto.
	 * @return Copia del contenido que llama al método.
	 */
	public Contenido copiar() {
		VotoContenido copia = new VotoContenido(null, _votante);
		super.copiarEstado(copia);
		return copia;
	}
	
}
