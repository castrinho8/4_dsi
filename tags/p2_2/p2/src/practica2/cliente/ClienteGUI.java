package practica2.cliente;

import javax.swing.SwingConstants;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import javax.swing.WindowConstants;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import java.awt.Insets;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.DefaultComboBoxModel;
import java.awt.Cursor;
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.JScrollPane;
import javax.swing.BoxLayout;
import java.awt.Container;
import javax.swing.Box;
import javax.swing.ListSelectionModel;

import practica2.servidor.Servidor;
import practica2.usuario.Usuario;



/**
 * Este es un cliente que implementa una interface gráfica de usuario
 * (GUI) en swing.
 */
public class ClienteGUI extends JFrame implements Cliente {

    public ClienteGUI() {
	_servidorActual = null;
	_comboModelServidores = new DefaultComboBoxModel();

	setJMenuBar(_menuBar());
	getContentPane().setLayout(new BorderLayout());
	getContentPane().add(_panelConsulta(), BorderLayout.CENTER);
	setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent event) {
		    _exitApp();
		}
	    });
	pack();
	//
	Dimension screenDim = getToolkit().getScreenSize();
	setLocation((screenDim.width - getSize().width)/2,
		    (screenDim.height - getSize().height)/2);
	setVisible(true);
	//
	conectar(null);
    }
    
    public Dimension getPreferredSize() {
	return new Dimension(850, 550);
    }

    private JPanel _panelConsulta() {
	JPanel panel = new JPanel(new GridBagLayout());
	//
	// -- Nombre: [________________]   [Buscar]
	JPanel panelNombre = new JPanel(new GridBagLayout());

	panelNombre.add(new JLabel("Nombre:"),
				     _gridBagConstraints(GridBagConstraints.CENTER,
							 GridBagConstraints.NONE,
							 0.0, 1.0, 0, 0));
	_textNombre = new JTextField();
	panelNombre.add(_textNombre,
			_gridBagConstraints(GridBagConstraints.CENTER,
					    GridBagConstraints.HORIZONTAL,
					    1.0, 1.0, 1, 0));
	JButton botonNombre = new JButton("Buscar");
	panelNombre.add(botonNombre,
			_gridBagConstraints(GridBagConstraints.CENTER,
					    GridBagConstraints.NONE,
					    0.0, 1.0, 2, 0));

	//
	// -- Servidor: [----------|\/|]
	JPanel panelServidor = new JPanel(new GridBagLayout());

	panelServidor.add(new JLabel("Servidor:"), 
				     _gridBagConstraints(GridBagConstraints.LINE_START,
							 GridBagConstraints.NONE,
							 0.0, 1.0, 0, 0));
	_comboServidores = new JComboBox(_comboModelServidores);
	panelServidor.add(_comboServidores,
			  _gridBagConstraints(GridBagConstraints.LINE_END,
					      GridBagConstraints.HORIZONTAL,
					      1.0, 1.0, 1, 0));

	//
	// -- Resultado: -----------------
	JPanel panelResultado = new JPanel(new GridBagLayout());
	panelResultado.add(new JLabel("Resultado:"),
			   _gridBagConstraints(GridBagConstraints.LINE_START,
					       GridBagConstraints.NONE,
					       0.0, 1.0, 0, 0));
	_etiquetaResultado = new JLabel("");
	panelResultado.add(_etiquetaResultado,
			   _gridBagConstraints(GridBagConstraints.LINE_END,
					       GridBagConstraints.HORIZONTAL,
					       1.0, 1.0, 1, 0));
	//
	// -- Usuario: -----------------
	JPanel panelUsuario = new JPanel(new GridBagLayout());
	_etiquetaUsuario = new JLabel("");
	_etiquetaUsuario.setHorizontalAlignment(SwingConstants.CENTER);
	panelUsuario.add(_etiquetaUsuario,
			 _gridBagConstraints(GridBagConstraints.CENTER,
					     GridBagConstraints.HORIZONTAL,
					     1.0, 1.0, 0, 0));
	
	//
	// --
	panel.add(panelUsuario, _gridBagConstraints(GridBagConstraints.NORTH,
						     GridBagConstraints.HORIZONTAL,
						     1.0, 0.0, 0, 0));
	panel.add(panelServidor, _gridBagConstraints(GridBagConstraints.NORTH,
						     GridBagConstraints.HORIZONTAL,
						     1.0, 0.0, 0, 1));
	panel.add(panelNombre, _gridBagConstraints(GridBagConstraints.NORTH,
						   GridBagConstraints.HORIZONTAL,
						   1.0, 0.0, 0, 2));
	panel.add(panelResultado, _gridBagConstraints(GridBagConstraints.NORTH,
						   GridBagConstraints.BOTH,
						   1.0, 1.0, 0, 3));
	//
	_comboServidores.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    _servidorActual = (Servidor)((JComboBox)e.getSource()).getSelectedItem();
		}
	    });
	botonNombre.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    ClienteGUI.this._buscar();
		}
	    });
	_textNombre.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    ClienteGUI.this._buscar();
		}
	    });
	return panel;
    }

    private JMenuBar _menuBar() {
	JMenuBar menuBar = new JMenuBar();
	JMenu fileMenu = new JMenu("Archivo");
	JMenu herramientasMenu = new JMenu("Herramientas");
	JMenu ayudaMenu = new JMenu("Ayuda");
	menuBar.add(fileMenu);
	menuBar.add(herramientasMenu);
	menuBar.add(ayudaMenu);
	JMenuItem salirAction = new JMenuItem("Salir");
	fileMenu.add(salirAction);
	JMenuItem configurarAction = new JMenuItem("Configurar");
	herramientasMenu.add(configurarAction);
	JMenuItem acercadeAction = new JMenuItem("Acerca de");
	ayudaMenu.add(acercadeAction);
	salirAction.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent event) {
		    ClienteGUI.this._exitApp();
		}
	    });
	configurarAction.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent event) {
		    new ConfigurarDialog(ClienteGUI.this, _comboModelServidores);
		}
	    });
	acercadeAction.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent event) {
		    JOptionPane.showMessageDialog(ClienteGUI.this, "p2 Servidor de nombres", "Acerca de", JOptionPane.INFORMATION_MESSAGE);
		}
	    });
	return menuBar;
    }

    public void añadirServidor(Servidor servidor) {
	_comboModelServidores.addElement(servidor);
    }

    public void conectar(Usuario u) {
	_usuario = u;
	if (_usuario != null) {
	    _etiquetaUsuario.setText("Conectado como " + _usuario);
	}
	else {
	    _etiquetaUsuario.setText("Ningún usuario conectado");
	}
    }

    private void _buscar() {
	String nombre = _textNombre.getText().trim();
	if ((!nombre.equals("")) && (_servidorActual != null) && (_usuario != null)) {
	    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
	    _etiquetaResultado.setText("Buscando ...");
	    String resultado = _servidorActual.resolver(_usuario, nombre);
	    setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	    if (resultado != null) {
		_etiquetaResultado.setText(resultado);
	    }
	    else {
		_etiquetaResultado.setText("-- No se encontro el nombre --");
	    }
	}
    }

    //------------------------------------------------------------  
    private GridBagConstraints _gridBagConstraints(int anchor, int fill, double weightx, double weighty, int gridx, int gridy) {
	GridBagConstraints gridBagConstraints = new GridBagConstraints();
	gridBagConstraints.anchor = anchor;
	gridBagConstraints.fill = fill;
	gridBagConstraints.weightx = weightx;
        gridBagConstraints.weighty = weighty;
        gridBagConstraints.gridx = gridx;
        gridBagConstraints.gridy = gridy;
        gridBagConstraints.insets = new Insets(8,8,8,8);
	return gridBagConstraints;
    }

    private void _exitApp() {
	System.out.println("Puede tener todo el dinero del mundo, pero hay algo que nunca podrá comprar.");
	dispose(); 
	System.exit(0);
    }

    //------------------------------------------------------------  
    private JTextField _textNombre;
    private JComboBox _comboServidores;
    private JLabel _etiquetaResultado;
    private JLabel _etiquetaUsuario;
    private DefaultComboBoxModel _comboModelServidores;

    private Servidor _servidorActual;
    private Usuario _usuario;

}




class ConfigurarDialog extends JDialog {
    public ConfigurarDialog(JFrame owner, ListModel servidores) {
	super(owner, "Configurar maestros", true);
	_servidores = new Servidor[servidores.getSize()];
	for(int i = 0; i<servidores.getSize(); i++) {
	    _servidores[i] = (Servidor) servidores.getElementAt(i);
	}
	_tableModel = new AbstractTableModel() {
		public int getColumnCount() { return 2; }
		public int getRowCount() { return ConfigurarDialog.this._servidores.length; }
		public String getColumnName(int col) {
		    switch(col) {
		    case 0: return "Servidor";
		    case 1: return "Maestro";
		    default: return "????";
		    }
		}
		public Object getValueAt(int row, int col) {
		    Servidor servidor = ConfigurarDialog.this._servidores[row];
		    switch(col) {
		    case 0: return servidor;
		    case 1: return servidor.obtenerMaestro();
		    default: return null;
		    }
		}
	    };

	setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent e) {
		    ConfigurarDialog.this._cerrar();
		}
	    });

	JPanel panelBotones = new JPanel();
	panelBotones.setLayout(new BoxLayout(panelBotones, BoxLayout.LINE_AXIS));
	JButton botonEstablecerMaestro = new JButton("Establecer");
	panelBotones.add(botonEstablecerMaestro);
	JButton botonQuitarMaestro = new JButton("Quitar");
	panelBotones.add(botonQuitarMaestro);
	panelBotones.add(Box.createHorizontalGlue());
	JPanel panelCerrar = new JPanel();
	panelCerrar.setLayout(new BoxLayout(panelCerrar, BoxLayout.LINE_AXIS));
	panelCerrar.add(Box.createHorizontalGlue());
	JButton botonCerrar = new JButton("Cerrar");
	panelCerrar.add(botonCerrar);

	Container panel = getContentPane();
	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	_tabla = new JTable(_tableModel);
	_tabla.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	panel.add(new JScrollPane(_tabla));
	panel.add(panelBotones);
	panel.add(panelCerrar);

	botonCerrar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    ConfigurarDialog.this._cerrar();
		}
	    });
	botonEstablecerMaestro.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    ConfigurarDialog.this._establecerMaestro(true);
		}
	    });
	botonQuitarMaestro.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    ConfigurarDialog.this._establecerMaestro(false);
		}
	    });

	pack();
	setLocationRelativeTo(owner);
	setVisible(true);
    }

    private void _cerrar() {
	setVisible(false);
    }

    private void _establecerMaestro(boolean esNuevo) {
	int idx = _tabla.getSelectedRow();
	if (idx == -1) {
	    JOptionPane.showMessageDialog(ConfigurarDialog.this, "Primero debes escoger el servidor.", "Error", JOptionPane.ERROR_MESSAGE);
	    return;
	}
	Servidor servidor = (Servidor) _tableModel.getValueAt(idx,0);
	Servidor maestro = null;
	if (esNuevo) {
	    maestro = (Servidor)
		JOptionPane.showInputDialog(this, "Selecciona el maestro para el servidor " + servidor,
					    "Establecer maestro", JOptionPane.QUESTION_MESSAGE, null,
					    _servidores, _servidores[0]);
	    if (maestro == null) {
		return;
	    }
	}
	servidor.establecerMaestro(maestro);
	_tableModel.fireTableCellUpdated(idx, 1);   
    }


    private Servidor _servidores[];
    private AbstractTableModel _tableModel;
    private JTable _tabla;
}
