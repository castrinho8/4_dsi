package practica2.usuario;

/**
 * Implementación de un usuario individual
 *
 */
public class UsuarioIndividual implements Usuario {

	/**
	 * Constructor de la clase que inicializa los atributos del usuario.
	 * @param nombre El nombre del usuario.
	 */
    public UsuarioIndividual(String nombre) {
        _nombre = nombre;
        _id = obtenerSiguienteID();
    }

    /**
     * Obtiene el siguiente ID.
     * @return El ID.
     */
    private int obtenerSiguienteID() {
        return _ultimoID++;
    }

    /**
     * Getter del nombre.
      * @return El nombre del usuario.
     */
    public String obtenerNombre() {
        return _nombre;
    }

    /**
     * Sobreescritura del metodo toString.
      * @return El toString.
     */
    public String toString() {
        return obtenerNombre();
    }

    /**
     * Getter del ID.
     * @return El ID del usuario.
     */
    public int obtenerID() {
        return _id;
    }

    /**
     * Funcion que comprueba la autorizacion de un usuario.
      * @param El nombre del usuario.
      * @return True si el ID del usuario pasado por parametro coincide con el del propio.
     */
    public boolean autorizar(Usuario otroUsuario) {
    	return (otroUsuario.obtenerID() == obtenerID());
    }

    //------------------------------------------------------------
    
    private String _nombre;
    private int _id;

    //------------------------------------------------------------
    //
    // atributo de clase, lo usamos para mantener un contador único

    private static int _ultimoID = 0;
}
