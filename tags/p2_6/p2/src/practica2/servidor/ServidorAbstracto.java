package practica2.servidor;

import practica2.usuario.Usuario;

public abstract class ServidorAbstracto implements Servidor {

	private Servidor maestro;

    /**
     * Obtiene el servidor maestro asociado con el servidor.
     *
     * @return servidor maestro. <code>null</code> si no tiene maestro
     */
	@Override
	public  Servidor obtenerMaestro() {
		return this.maestro;
	}

    /**
     * Establece el servidor maestro asociado con el servidor.
     *
     * @param maestro servidor maestro que será asociado
     */
	@Override
	public void establecerMaestro(Servidor maestro) {
		this.maestro = maestro;
	}
	
    /**
     * Obtiene el nombre del servidor.
     *
     * @return nombre del servidor
     */
	@Override
	public abstract String obtenerNombre();

    /**
     * Servicio de resolución de nombres, devuelve la cadena de texto
     * asociada a una clave dada.
     *
     * @param usuario usuario que realiza la petición
     * @param clave nombre a ser resuelto por el servidor
     * @return valor asociado con la clave. "-- Acceso No Autorizado --" si no se puede resolver
     */
	@Override
	public abstract String resolver(Usuario usuario, String clave);

}
