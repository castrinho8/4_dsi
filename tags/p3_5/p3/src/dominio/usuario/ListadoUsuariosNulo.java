package dominio.usuario;

/**
 * Clase que implementa la interfaz ListadoUsuarios y que 
 * modela una lista de usuarios vacia.
 *
 */
public class ListadoUsuariosNulo implements ListadoUsuarios {
	
	/**
	 * Función que devuelve el siguiente usuario en la lista.
	 * En este caso devolverá el valor NULL.
	 * @return El usuario siguiente (NULL).
	 */
	public Usuario siguiente() {
		return null;
	}
	
}
