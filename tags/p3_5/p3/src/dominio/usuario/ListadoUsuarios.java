package dominio.usuario;

/**
 * Interfaz de una lista de usuarios.
 *
 */
public interface ListadoUsuarios {
	
	/**
	 * Función que devuelve el siguiente usuario en la lista.
	 * @return El usuario siguiente.
	 */
	public Usuario siguiente();
	
}
