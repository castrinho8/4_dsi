package dominio;

import java.util.Collection;
import java.util.Vector;

import util.Interesante;
import dominio.contenido.Contenido;
import dominio.contenido.FiltroContenidos;
import dominio.contenido.ListadoContenidos;
import dominio.contenido.ListadoContenidosColeccion;
import dominio.contenido.complemento.VotoContenido;
import dominio.contenido.elemento.Elemento;
import dominio.contenido.elemento.ListadoElementos;
import dominio.contenido.elemento.ListadoElementosColeccion;
import dominio.contenido.especificacion.EspecificacionContenido;
import dominio.contenido.especificacion.ListadoEspecificaciones;
import dominio.contenido.especificacion.ListadoEspecificacionesColeccion;
import dominio.usuario.Usuario;

/**
 * Clase que guarda y provee los contenidos de la wiki.
 * Es una subclase de Interesante
 * @see Interesante
 * 
 */
public class GestorContenidos extends Interesante {
	
	private Collection<Elemento> _plantillas;
	private Collection<Contenido> _contenidos;
	private Collection<EspecificacionContenido> _especificaciones;
	private static GestorContenidos _instancia = new GestorContenidos();
	
	/**
	 * Constructor privado de la clase.
	 * Inicializa las colecciones de las plantillas, de los contenidos y de 
	 * las especificaciones de contenidos de la wiki.
	 */
	private GestorContenidos() {
		_plantillas = new Vector<Elemento>();
		_contenidos = new Vector<Contenido>();
		_especificaciones = new Vector<EspecificacionContenido>();
	}
	
	/**
	 * Método que devuelve una instacia de la clase. Esta clase implementa el patrón <i>singleton</i>.
	 * @return Instancia unica de la clase.
	 */
	public static GestorContenidos instancia() {
		return _instancia;
	}
	
	/**
	 * Método que permite agregar una nueva plantilla.
	 * Asi mismo, avisa del cambio a los objetos que estén interesados.
	 * @param plantilla Plantilla a agregar.
	 */
	public void agregarPlantilla(Elemento plantilla) {
		_plantillas.add(plantilla);
		avisarInteresados();
	}
	
	/**
	 * Método que devuelve las plantillas guardadas.
	 * @return Un listado de plantillas.
	 */
	public ListadoElementos obtenerPlantillas() {
		return new ListadoElementosColeccion(_plantillas);
	}
	
	/**
	 * Método que devuelve las especificaciones guardadas.
	 * @return Un listado de especificaciones.
	 */
	public ListadoEspecificaciones obtenerEspecificaciones() {
		return new ListadoEspecificacionesColeccion(_especificaciones);
	}
	
	/**
	 * Método que devuelve los contenidos almacenados en el sistema que cumplen una determinada especificación.
	 * @param especificacion Especificacion que debe cumplir los contenidos listados.
	 * @return Un listado de contenidos.
	 */
	public ListadoContenidos obtenerContenidos(EspecificacionContenido especificacion) {
		return new FiltroContenidos(
				new ListadoContenidosColeccion(_contenidos), especificacion);
	}
	
	/**
	 * Método que permite crear un nuevo contenido y almacenarlo en el sistema.
	 * @param plantilla Plantilla de la que se parte.
	 * @param propietario Propietario del nuevo contenido.
	 * @param titulo Título del contenido.
	 */
	public void crearContenido(Elemento plantilla, Usuario propietario, String titulo) {
		Contenido contenido = plantilla.copiar();
		contenido.establecerPropietario(propietario);
		contenido.establecerTitulo(titulo);
		agregarContenido(contenido);
	}
	
	/**
	 * Método que permite agregar un nuevo contenido al sistema.
	 * Asi mismo, avisa del cambio a los elementos que estén interesados.
	 * @param contenido Contenido a agregar.
	 */
	public void agregarContenido(Contenido contenido) {
		_contenidos.add(contenido);
		avisarInteresados();
	}
	
	/**
	 * Método que permite agregar unha nova especificación o sistema.
	 * Asi mismo, avisa del cambio a los elementos que estén interesados.
	 * @param especificacion Especificacion a agregar.
	 */
	public void agregarEspecificacion(EspecificacionContenido especificacion) {
		_especificaciones.add(especificacion);
		avisarInteresados();
	}
	
	/**
	 * Método que permite que un usuario vote por un determinado contido.
	 * Asi mismo, avisa del cambio a los elementos que estén interesados.
	 * @param contenido Contido polo que o usuario vota.
	 * @param usuario Usuario que vota.
	 */
	public void votar(Contenido contenido, Usuario usuario) {
		if ((!contenido.valorado(usuario)) && (_contenidos.remove(contenido))) {
			_contenidos.add(new VotoContenido(contenido, usuario));
			avisarInteresados();
		}
	}
	
}
