package dominio.contenido.especificacion;

import java.util.Collection;
import java.util.Vector;

/**
 * Clase que implementa la interfaz ListadoEspecificaciones y que 
 * modela una colección de especificaciones de contenidos.
 *
 */
public class ListadoEspecificacionesColeccion implements ListadoEspecificaciones {
	
	/**
	 * Constructor de la clase ListadoEspecificacionesColeccion.
	 * Inicializa un vector con las especificaciones que se le pasan por parámetro.
	 * @param especificaciones La colección de especificaciones que formará la colección.
	 */
	public ListadoEspecificacionesColeccion(Collection<EspecificacionContenido> especificaciones) {
		_especificaciones = new Vector<EspecificacionContenido>(especificaciones);
		_posicion = 0;
	}
	
	/**
	 * Función que devuelve la siguiente especificación en la lista.
	 * @return La especificación siguiente.
	 */
	public EspecificacionContenido siguiente() {
		if (_posicion < _especificaciones.size()) {
			return _especificaciones.elementAt(_posicion++);
		} else {
			return null;
		}
	}
	
	private Vector<EspecificacionContenido> _especificaciones;
	
	private int _posicion;
	
}
