package dominio.contenido.especificacion;

import dominio.contenido.Contenido;
/**
 * Clase que implementa una función NOT de una Especificacion.
 */
public class EspecificacionNOT implements EspecificacionContenido {

	private EspecificacionContenido especificacion;

	/**
	 * Constructor de la clase.
	 * Inicializa un vector con las especificaciones que se le pasan por parámetro.
	 * @param especificacion Especificacion del la que se hace NOT.
	 */
	public EspecificacionNOT(EspecificacionContenido especificacion) {
		this.especificacion = especificacion;
	}

	/**
	 * Metodo que aporta una descripción del filtro.
	 * @return Un String con una descripcion del filtro.
	 */
	@Override
	public String descripcion() {
		return "NOT " + this.especificacion.descripcion();
	}

	/**
	 * Método que indica si el contenido pasado por parametro esta dentro del filtro o no.
	 * @param contenido Instancia del contenido que se quiere comprobar.
	 * @return El booleano <em>true</em> si el contenido pasado por parametro no es valido para la especificacion del filtro. Devuelve <em>false</em> en caso contrario.
	 */
	@Override
	public boolean valido(Contenido contenido) {
		return ! this.especificacion.valido(contenido);
	}

	/**
	 * Método toString de la clase.
	 * @return un String con la descripción del filtro.
	 */
	@Override
	public String toString() {
		return this.descripcion();
	}

}
