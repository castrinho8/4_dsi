package dominio.contenido;

/**
 * Interfaz de una lista de contenidos.
 */
public interface ListadoContenidos {
	
	/**
	 * Método que devuelve el siguiente elemento de la lista.
	 * @return El siguiente Contenido de la lista.
	 */
	public Contenido siguiente();
	
}
