package dominio.contenido.elemento;

import dominio.contenido.Contenido;
import dominio.usuario.Usuario;

/**
 * Clase que modela la cabecera de una entrada de un artículo.
 * Es una subclase de ElementoCompuesto.
 * @see ElementoCompuesto
 *
 */
public class Cabecera extends ElementoCompuesto {
	
	/**
	 * Constructor de la clase Cabecera.
	 * Inicializa el propietario introducido por parámetro y llama al constructor de la
	 * superclase pasándole el título.
	 * @param titulo El título.
	 * @param propietario El usuario propietario del articulo.
	 */
	public Cabecera(String titulo, Usuario propietario) {
		super(titulo);
		_propietario = propietario;
	}
	
	/**
	 * Getter del propietario.
	 * @return El propietario del artículo.
	 */
	public Usuario obtenerPropietario() {
		return _propietario;
	}
	
	/**
	 * Setter del propietario.
	 * @param propietario El nuevo propietario del artículo.
	 */
	public void establecerPropietario(Usuario propietario) {
		_propietario = propietario;
	}
	
	/**
	 * Getter del titulo.
	 * @return El String correspondiente al texto que forma el título.
	 */
	public String obtenerTitulo() {
		return obtenerTexto();
	}
	
	/**
	 * Setter del titulo
	 * @param titulo El nuevo titulo a establecer.
	 */
	public void establecerTitulo(String titulo) {
		establecerTexto(titulo);
	}
	
	/**
	 * Método que agrega el elemento pasado por parámetro al elemento que 
	 * llama al método.
	 * @param elemento El elemento a agregar.
	 * @throws OperacionInvalida En caso de que este elemento no puede alojar a una cabecera.
	 */
	public void agregar(Elemento elemento) throws OperacionInvalida {
		if (elemento instanceof Cabecera) {
			throw new OperacionInvalida("Este elemento no puede alojar una cabecera");
		} else {
			super.agregar(elemento);
		}
	}
	
	/**
	 * Método que propone el elemento pasado por parámetro para su eliminación.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha proposición.
	 * Implementación por defecto que devuelve una excepción OperacionInvalida ya que la cabecera 
	 * es un elemento que no se puede borrar.
	 * @param editor El usuario que propone el borrado.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la edición.
	 */
	public void proponerBorrado(Usuario editor) throws OperacionInvalida {
		throw new OperacionInvalida("La cabecera no es un elemento borrable");
	}
	
	/**
	 * Método que obtiene el texto de la cabecera correctamente formateado para su impresión.
	 * Devuelve el título de la cabecera y llama a la superclase con el mísmo método por si hay 
	 * más objetos en su interior.
	 * @param seccion La sección.
	 * @return El string con la versión imprimible.
	 */
	public String obtenerVersionImprimible(String seccion) {
		return "--------------------------------------------------------------------------------\n"
		     + obtenerTitulo() + "\n"
		     + "--------------------------------------------------------------------------------\n\n\n"
		     + super.obtenerVersionImprimible(seccion);
	}
	
	/**
	 * Método que copia este elemento en otro y devuelve la copia.
	 * @return Un elemento que es copia del que llama al método.
	 */
	public Contenido copiar() {
		Cabecera copia = new Cabecera(obtenerTitulo(), _propietario);
		super.copiarEstado(copia);
		return copia;
	}
	
	private Usuario _propietario;
	
}
