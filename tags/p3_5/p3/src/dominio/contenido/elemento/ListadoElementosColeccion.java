package dominio.contenido.elemento;

import java.util.Collection;
import java.util.Vector;

/**
 * Clase que implementa la interfaz ListadoElementos y que 
 * modela una colección de elementos.
 *
 */
public class ListadoElementosColeccion implements ListadoElementos {
	
	/**
	 * Constructor de la clase ListadoElementosColeccion.
	 * Inicializa un vector de Elementos a la coleccion introducida por parámetro..
	 * @param elementos La colección de elementos con la que inicializar el objeto.
	 */
	public ListadoElementosColeccion(Collection<Elemento> elementos) {
		_elementos = new Vector<Elemento>(elementos);
		_posicion = 0;
	}
	
	/**
	 * Función que devuelve el elemento siguiente en la lista.
	 * @return El elemento siguiente.
	 */
	public Elemento siguiente() {
		if (_posicion < _elementos.size()) {
			return _elementos.elementAt(_posicion++);
		} else {
			return null;
		}
	}
	
	private Vector<Elemento> _elementos;
	
	private int _posicion;
	
}
