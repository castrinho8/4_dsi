package dominio.contenido.elemento;

import dominio.Buzon;
import dominio.usuario.Usuario;

/**
 * Clase que modela un elemento pendiente de aprobación.
 * Subclase de ElementoPendiente
 * @see ElementoPendiente
 *
 */
public class ElementoPendienteAprobacion extends ElementoPendiente {
	
	/**
	 * Constructor de la clase ElementoPendienteAprobacion.
	 * Inicializa la revisión a la revisión que recibe por parámetro y llama al construtor de 
	 * la superclase pasandole el elemento y el editor.
	 * @param elemento El elemento a aprobar.
	 * @param editor El editor correspodiente.
	 * @param revision La revisión que se ha realizado.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos.
	 */
	public ElementoPendienteAprobacion(Elemento elemento, Usuario editor, String revision)
	throws OperacionInvalida {
		super(elemento, editor);
		_revision = revision;
	}
	
	/**
	 * Getter de la revisión de un Elemento Pendiente de Aprobación.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha acción.
	 * @param elemento El elemento del que obtener la revisión.
	 * @param usuario El usuario que obtiene la revisión.
	 * @return El String con la revisión.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos.
	 */
	public String obtenerRevision(Elemento elemento, Usuario usuario)
	throws OperacionInvalida {
		if (elemento.autorizado(usuario)) {
			return _revision;
		} else {
			throw new OperacionInvalida("No tiene permisos para ejecutar esta operación");
		}
	}
	
	/**
	 * Método que devuelve el codigo que indica la situación en la que se encuentra
	 * el elemento.
	 * @param elemento El elemento.
	 * @return El codigo de situacion, en este caso "PA"
	 */
	public String codigoSituacion(Elemento elemento) {
		return "PA";
	}
	
	/**
	 * Método que realiza la aprobación de un elemento pendiente de aprobación.
	 * @param elemento El elemento a aprobar.
	 */
	protected void aprobarTransicion(Elemento elemento)
	throws OperacionInvalida {
		elemento.establecerTexto(_revision);
		elemento.establecerSituacion(new ElementoVisible());
	}
	
	/**
	 * Método que agrega una nueva revisión al buzon.
	 * @param revisor El revisor a agregar.
	 * @param elemento El elemento a agregar.
	 */
	protected void agregarMensaje(Usuario revisor, Elemento elemento) {
		Buzon.instancia().agregarRevision(revisor, elemento);
	}
	
	/**
	 * Método que elimina una revisión del buzón.
	 * @param revisor El revisor a agregar.
	 * @param elemento El elemento a agregar.
	 */
	protected void eliminarMensaje(Usuario revisor, Elemento elemento) {
		Buzon.instancia().eliminarRevision(revisor, elemento);
	}
	
	/**
	 * Método que devuelve la descripción de la situación.
	 * @return La descripción de la situación, en este caso "pendiente aprobación"
	 */
	protected String descripcion() {
		return "pendiente aprobación";
	}
	
	private String _revision;
	
}
