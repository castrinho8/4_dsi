package practica2.usuario;

/**
 * Esta es la interfaz que representa la entrada al subsistema usuario.
 * Un usuario permite obtener su nombre (<code>obtenerNombre</code>),
 * obtener un identificador (nº entero) que identifique de forma única al
 * usuario (<code>obtenerID</code>), y un método que permite autorizar o no a
 * otro usuario (<code>autorizar</code>).
 */

public interface Usuario {
	
    /**
     * Obtiene el nombre del usuario.
     *
     * @return nombre del usuario
     */
    public String obtenerNombre();


    /**
     * Obtiene el identificador del usuario.
     *
     * @return identificador del usuario
     */
    public int obtenerID();


    /**
     * Determina si el usuario autoriza a otro usuario.
     *
     * @param otroUsuario usuario a ser autorizado
     * @return true si el usuario autoriza al otro usuario
     */
    public boolean autorizar(Usuario otroUsuario);

}
