package practica2.servidor;

import practica2.usuario.Usuario;

/**
 * Esta interface sirve de punto de entrada al subsistema de servidores.
 * Cada servidor proporciona una servicio de resolución de nombres
 * (<code>resolver</code>, así como la posibilidad de obtener y establecer
 * un servidor maestro asociado (<code>obtenerMaestro</code>, 
 * <code>establecerMaestro</code>), y el nombre del servidor
 * (<code>obtenerNombre</code>).
 */

public interface Servidor {

    /**
     * Obtiene el nombre del servidor.
     *
     * @return nombre del servidor
     */
    public String obtenerNombre();


    /**
     * Obtiene el servidor maestro asociado con el servidor.
     *
     * @return servidor maestro. <code>null</code> si no tiene maestro
     */
    public Servidor obtenerMaestro();


    /**
     * Establece el servidor maestro asociado con el servidor.
     *
     * @param maestro servidor maestro que será asociado
     */
    public void establecerMaestro(Servidor maestro);


    /**
     * Servicio de resolución de nombres, devuelve la cadena de texto
     * asociada a una clave dada.
     *
     * @param usuario usuario que realiza la petición
     * @param clave nombre a ser resuelto por el servidor
     * @return valor asociado con la clave. "-- Acceso No Autorizado --" si no se puede resolver
     */
    public String resolver(Usuario usuario, String clave);

}
