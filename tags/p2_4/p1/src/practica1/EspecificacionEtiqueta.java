package practica1;

/**
 * Especificacion en funcion de la etiqueta de la alarma.
 *
 * @see Especificacion
 */
public class EspecificacionEtiqueta implements Especificacion{

	private Etiqueta etiq;

	/**
	 * Getter de la etiqueta para la cual es valida esta especificacion.
	 * @return La etiqueta.
	 */
	public Etiqueta getEtiqueta() {
		return this.etiq;
	}
	
	/**
	 * Metodo que comprueba si una alarma cumple los requisitos o no.
	 * @param a La alarma a comprobar.
	 * @return El valor booleano true si cumple los requisitos y false en caso contrario.
	 */
	public boolean validar(Alarma a){
		return a.getEtiquetas().contains(etiq);
	}
	
}
