package practica2;

import practica2.cliente.*;
import practica2.servidor.*;
import practica2.usuario.*;

public class Main {

    public static void main(String argv[]) {
		Usuario gulias = new UsuarioIndividual("Gulías");
		Servidor imperio = new ControlAcceso(new LogAcceso(new ProxyCache("Imperiales", "imperio.dat")),gulias);
		Servidor rebeldes = new LogAcceso(new ControlAcceso(new ProxyCache("Rebeldes", "rebeldes.dat"),new UsuarioIndividual("Juan")));
		Cliente c = new ClienteGUI();
		c.añadirServidor(imperio);
		c.añadirServidor(rebeldes);
		c.conectar(gulias);
    }

}
