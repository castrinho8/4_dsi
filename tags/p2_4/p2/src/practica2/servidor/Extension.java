package practica2.servidor;

import practica2.usuario.Usuario;

/**
 * Clase que modela las funciones de acceso al sistema y
 * que delega en el componente los métodos. 
 *
 */
public abstract class Extension implements Servidor{
	
	private Servidor componente;
	
	/**
	 * Constructor de la clase
	 * @param componente Servidor para el que se guarda el log.
	 */
	public Extension(Servidor componente) {
		super();
		this.componente = componente;
	}
	
    /**
     * Obtiene el nombre del servidor.
     *
     * @return nombre del servidor
     */
    public String obtenerNombre(){
    	return componente.obtenerNombre();
    }

    /**
     * Obtiene el servidor maestro asociado con el servidor.
     *
     * @return servidor maestro. <code>null</code> si no tiene maestro
     */
    public Servidor obtenerMaestro(){
    	return componente.obtenerMaestro();
    }


    /**
     * Establece el servidor maestro asociado con el servidor.
     *
     * @param maestro servidor maestro que será asociado
     */
    public void establecerMaestro(Servidor maestro){
    	componente.establecerMaestro(maestro);
    }


    /**
     * Servicio de resolución de nombres, devuelve la cadena de texto
     * asociada a una clave dada.
     *
     * @param usuario usuario que realiza la petición
     * @param clave nombre a ser resuelto por el servidor
     * @return valor asociado con la clave. "-- Acceso No Autorizado --" si no se puede resolver
     */
    public String resolver(Usuario usuario, String clave){
    	return componente.resolver(usuario, clave);
    }

    /**
     * Sobrescritura del método toString
     */
    @Override
    public String toString(){
    	return componente.toString();
    }
    
}
