public class Main {

    public static void main(String argv[]) {
	Usuario gulias = new UsuarioIndividual("Gulías");
	Servidor imperio = new ServidorReal("Imperiales", "imperio.dat");
	Servidor rebeldes = new ServidorReal("Rebeldes", "rebeldes.dat");
	Cliente c = new ClienteGUI();
	c.añadirServidor(imperio);
	c.añadirServidor(rebeldes);
	c.conectar(gulias);
    }

}
