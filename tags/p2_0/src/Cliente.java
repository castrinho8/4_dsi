/**
 * Esta es la interface del cliente de los subsistemas servidor y
 * usuario.  Cada cliente maneja una lista de servidores que se le
 * indican (<code>añadirServidor</code>). Todas las peticiones se
 * realizan por parte de un usuario que se debe especificar
 * previamente (<code>conectar</code>).
 */

public interface Cliente {

    /**
     * Agrega un servidor a los servidores conocidos por el cliente.
     *
     * @param servidor servidor a añadir
     */
    public void añadirServidor(Servidor servidor);

    /**
     * Conecta un usuario al cliente para poder realizar las peticiones
     * a los servidores conocidos.
     *
     * @param usuario usuario que se conecta a través del cliente
     */
    public void conectar(Usuario usuario);
}
