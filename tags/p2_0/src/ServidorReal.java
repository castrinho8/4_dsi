import java.util.Hashtable;
import java.io.*;

/**
 * Esta es una implementación de la interfaz Servidor que almacena
 * en una tabla de dispersion pares (clave,valor) tomados de un fichero.
 * Para ello se usa el paquete @see{java.util.Hashtable}.
 * No se implementa el concepto de <i>maestro</i>.
 *
 * La operación de resolver incluye de forma deliberada una espera para
 * simular un servicio costoso en tiempo.
 */

public class ServidorReal implements Servidor {

    /**
     * El constructor inicializa el nombre del servidor y la tabla hash,
     * para después cargar los pares (clave,valor) de un fichero
     *
     * @param nombre nombre de servidor
     * @param fichero nombre del fichero con los pares (clave,valor)
     */

    public ServidorReal(String nombre, String fichero) {
        _nombre = nombre;
        _diccionario = new Hashtable<String,String>();
        cargaDiccionario(fichero);
    }


    /**
     * Carga los pares en la tabla hash de un fichero de texto con el formato
     * <code>
     *    clave1\n
     *    valor1\n
     *    clave2\n
     *    valor2\n
     *    ...
     *    claven\n
     *    valorn\n
     * </code>
     * Para la entrada salida se hace uso de las clases definidas 
     * en @see{java.io}.*. 
     *
     * @param fichero nombre del fichero con los pares (clave,valor)
     */

    private void cargaDiccionario(String fichero) {
        try {
            BufferedReader f = new BufferedReader(new FileReader(fichero));
            
            String clave = f.readLine();
            while (clave != null) {
                _diccionario.put(clave, f.readLine());
                clave = f.readLine();
            }

            f.close();
            
        } catch (FileNotFoundException e) {
            System.err.println("fichero " + fichero
                               + " no encontrado. Diccionario vacío");
        } catch (IOException e) { 
            System.err.println( "Error en lectura del diccionario "
                                + fichero + ": " + e.getMessage() ); 
        }
    }


    /**
     * Devuelve el nombre del servidor, inicializado en el constructor.
     *
     * @return nombre del servidor
     */
    public String obtenerNombre() {
        return _nombre;
    }

    public String toString() {
        return obtenerNombre();
    }


    /**
     * Operación no implementada (devuelve el maestro asociado al servidor).
     *
     * @return devuelve siempre <code>null</code>
     */
    public Servidor obtenerMaestro() {
	return null;
    }


    /**
     * Operación no implementada (establece el maestro asociado al servidor).
     *
     * @param maestro maestro propuesto para el servidor
     */
    public void establecerMaestro(Servidor maestro) { }


    /**
     * Resolución de nombre mediante búsqueda en la tabla hash.
     * Para simular una operación costosa, penalizamos la operación con
     * 5 segundos de espera.
     *
     * @param usuario usuario que realiza la petición
     * @param clave clave a buscar
     * @return valor asociado a la clave. <code>null</code> si no existe
     */
    public String resolver(Usuario usuario, String clave) {
        try { Thread.sleep(5000); } catch (Exception e) {}
        return _diccionario.get(clave);
    }


    //------------------------------------------------------------  

    private Hashtable<String,String> _diccionario;
    private String _nombre;
}
