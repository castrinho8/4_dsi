package practica1;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Entidad que modela las centralitas de alarmas.
 * 
 * @see Alarma 
 */
public class Centralita {
	private boolean autenticado;
	private String contrasenha;
	private ArrayList<Alarma> alarmas;

	/**
	 * Constructor de la clase.
	 * @param contrasenha La contrasenha para acceder a las funciones de la centralita.
	 */
	public Centralita(String contrasenha) {
		super();
		this.autenticado = false;
		this.contrasenha = contrasenha;
		this.alarmas = new ArrayList<Alarma>();
	}

	/**
	 * Anhade una alarma a la centralita.
	 * @param a La alarma a anhadir.
	 */
	public void agregarAlarma(Alarma a){
		if(!autenticado) return;
		alarmas.add(a);
	}
	
	/**
	 * Comprueba la correcta autentificacion para el acceso a las funcionalidades de la centralita.
	 * @param c La contrasenha a validar.
	 * @return El valor True si la contrasenha coincide con la proporcionada y False en caso contrario.
	 */
	public boolean autenticar(String c){
		if (c == null){
			return false;
		}
		if (this.contrasenha.equals(c)){
			this.autenticado = true;
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Realiza una busqueda en funcion de la especificacion pasada por parametro.
	 * @param e La especificacion utilizada en la busqueda.
	 * @return Coleccion de alarmas que cumple la especificacion.
	 */
	public Collection<Alarma> buscar(Especificacion e){
		if(!autenticado) return null;
		Collection<Alarma> c = new ArrayList<Alarma>();
		for(Alarma a : alarmas){
			if(e.validar(a)){
				c.add(a);
			}
		}
		return c;
	}
	
	/**
	 * Elimina la alarma indentificada por el id pasado por parametro.
	 * @param id El identificador de la alarma a eliminar.
	 */
	public void eliminarAlarma(String id){
		if(!autenticado) return;
		for(Alarma a : alarmas) {
			if(a.id_alarma().equals(id)) {
				alarmas.remove(alarmas.indexOf(a));
			}
		}
	}
	
	/**
	 * Activa todas las alarmas de la centralita.
	 */
	public void probar(){
		if(!autenticado) return;
		for(Alarma a : alarmas){
			a.probar();
		}
	}
	
	/**
	 * Reinicia todas las alarmas de la centralita.
	 */
	public void reiniciar(){
		if(!autenticado) return;
		for(Alarma a : alarmas){
			a.reiniciar();
		}
	}
	
}
