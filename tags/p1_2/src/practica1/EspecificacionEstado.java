package practica1;

/**
 * Especificacion en funcion del estado de la alarma.
 *
 * @see Especificacion
 */
public class EspecificacionEstado implements Especificacion{
		
	private boolean estado;

	/**
	 * Contructor de la clase.
	 * @param estado El estado para el cual es valido esta especificacion.
	 */
	public EspecificacionEstado(boolean estado) {
		super();
		this.estado = estado;
	}

	/**
	 * Getter del estado de la especificacion. 
	 * @return El estado.
	 */
	public boolean getEstado() {
		return estado;
	}
	
	/**
	 * Comprueba si una alarma cumple los requisitos.
	 * @param a La alarma a comprobar.
	 * @return El valor booleano true si cumple los requisitos y false en caso contrario.
	 */
	public boolean validar(Alarma a){
		return a.alerta() == estado;
	}
	
}
