package dominio.usuario;

import java.util.Vector;

/**
 * Clase que implementa la interfaz ListadoUsuarios y que 
 * modela una colección de usuarios.
 *
 */
public class ListadoUsuariosColeccion implements ListadoUsuarios {
	
	/**
	 * Constructor de la clase ListadoUsuariosColeccion.
	 * @param usuarios El vector de usuarios que formará la colección.
	 */
	public ListadoUsuariosColeccion(Vector<Usuario> usuarios) {
		_usuarios = usuarios;
		_posicion = 0;
	}
	
	/**
	 * Función que devuelve el siguiente usuario en la lista.
	 * @return El usuario siguiente.
	 */
	public Usuario siguiente() {
		if (_posicion < _usuarios.size()) {
			return _usuarios.elementAt(_posicion++);
		} else {
			return null;
		}
	}
	
	private Vector<Usuario> _usuarios;
	
	private int _posicion;
	
}
