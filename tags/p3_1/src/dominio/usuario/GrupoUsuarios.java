package dominio.usuario;

import java.util.Vector;

/**
 * Clase que modela un grupo como conjunto de usuarios.
 *
 */
public class GrupoUsuarios extends Usuario {
	
	/**
	 * Constructor de la clase GrupoUsuarios.
	 * @param nombre El nombre que se le va a dar al nuevo grupo.
	 */
	public GrupoUsuarios(String nombre) {
		super(nombre);
		_usuarios = new Vector<Usuario>();
	}
	
	/**
	 * Añade un usuario al grupo.
	 * @param usuario El usuario a añadir.
	 */
	public void agregar(Usuario usuario) {
		_usuarios.addElement(usuario);
	}
	
	/**
	 * Quita a un usuario del grupo.
	 * @param usuario El usuario a eliminar.
	 */
	public void eliminar(Usuario usuario) {
		_usuarios.removeElement(usuario);
	}
	
	/**
	 * Devuelve el usuario del grupo que se encuentra en la posición indicada.
	 * @param posicion El indice de la lista en la que se encuentra el usuario.
	 * @return El usuario correspondiente.
	 */
	public Usuario obtenerUsuario(int posicion) {
		return _usuarios.elementAt(posicion);
	}
	
	/**
	 * Obtiene el número total de miembros pertenecientes al grupo.
	 * @return El número de miembros del grupo.
	 */
	public int obtenerNumeroMiembros() {
		int resultado = 0;
		for (int i = 0; i < _usuarios.size(); i++) {
			resultado += obtenerUsuario(i).obtenerNumeroMiembros();
		}
		return resultado;
	}
	
	/**
	 * Obtiene una coleccion con la lista de miembros de un grupo.
	 * @return La lista de miembros.
	 */
	public ListadoUsuarios obtenerMiembros() {
		return new ListadoUsuariosColeccion(obtenerListadoMiembros());
	}
	
	/**
	 * Función que recorre el grupo comprobando si el usuario indicado por parámetro
	 * se encuentra en él o no.
	 * @param usuario El usuario a buscar.
	 * @return True en caso de que se encuentre y False en caso contrario.
	 */
	public boolean contiene(Usuario usuario) {
		if (!equals(usuario)) {
			for (int i = 0; i < _usuarios.size(); i++) {
				if (obtenerUsuario(i).contiene(usuario)) {
					return true;
				}
			}
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Devuelve una lista con los miembros del grupo.
	 * @return La lista con los miembros pertenecientes al grupo.
	 */
	protected Vector<Usuario> obtenerListadoMiembros() {
		Vector<Usuario> resultado = new Vector<Usuario>();
		for (int i = 0; i < _usuarios.size(); i++) {
			resultado.addAll(obtenerUsuario(i).obtenerListadoMiembros());
		}
		return resultado;
	}
	
	private Vector<Usuario> _usuarios;
	
}


