package dominio.contenido.elemento;

/**
 * Clase que modela un listado de elementos vacio.
 *
 */
public class ListadoElementosNulo implements ListadoElementos {

	/**
	 * Función que devuelve el elemento siguiente en la lista.
	 * En este caso devolverá el valor NULL.
	 * @return El elemento siguiente (NULL).
	 */
	public Elemento siguiente() {
		return null;
	}
	
}
