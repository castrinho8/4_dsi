package dominio.contenido.elemento;

import java.util.Vector;

/**
 * Clase que modela un elemento que se encuentra compuesto por varios elementos.
 *
 */
public abstract class ElementoCompuesto extends Elemento {
	
	/**
	 * Constructor de la clase ElementoCompuesto
	 * @param texto El texto 
	 */
	public ElementoCompuesto(String texto) {
		super(texto);
		_elementos = new Vector<Elemento>();
	}
	
	/**
	 * Método que agrega el elemento pasado por parámetro al elemento que 
	 * llama al método.
	 * @param elemento El elemento a agregar.
	 * @throws OperacionInvalida En caso de que este elemento no puede alojar a otros elementos.
	 */
	public void agregar(Elemento elemento) throws OperacionInvalida {
		if (elemento.obtenerPadre() != null) {
			elemento.obtenerPadre().eliminar(elemento);
		}
		_elementos.addElement(elemento);
		elemento.establecerPadre(this);
		elemento.agregarInteresado(this);
		avisarInteresados();
	}
	
	/**
	 * Método que elimina el elemento pasado por parámetro del elemento que
	 * llama al método.
	 * @param elemento El elemento a eliminar.
	 * @throws OperacionInvalida En caso de que este elemento no aloje a otros elementos.
	 */
	public void eliminar(Elemento elemento) throws OperacionInvalida {
		if (elemento.obtenerPadre() == this) {
			elemento.establecerPadre(null);
			_elementos.removeElement(elemento);
			elemento.eliminarInteresado(this);
			avisarInteresados();
		}
	}
	
	/**
	 * Método que obtiene el elemento que se encuentra 
	 * en la posición indicada por parámetro.
	 * @param posicion La posición en la que se encuentra el elemento.
	 * @return El elemento a recuperar.
	 */
	public Elemento obtenerElemento(int posicion) throws OperacionInvalida {
		return _elementos.elementAt(posicion);
	}
	
	/**
	 * Método que obtiene los elementos de los que se compone este elemento. 
	 * @return El listado de elementos.
	 */
	public ListadoElementos obtenerElementos() {
		return new ListadoElementosColeccion(_elementos);
	}
	
	/**
	 * Método que obtiene el texto de un elemento compuesto y de todos
	 * los elementos que lo forman, correctamente formateados para su impresión.
	 * @param seccion La sección.
	 * @return El string con la versión imprimible.
	 * @throws RuntimeException En caso de que la operación no sea válida.
	 */
	public String obtenerVersionImprimible(String seccion) {
		try {
			String resultado = "";
			for (int i = 0; i < _elementos.size(); i++) {
				resultado += obtenerElemento(i).obtenerVersionImprimible(seccion + (i + 1) + ".") + "\n\n";
			}
			return resultado;
		} catch (OperacionInvalida op) {
			throw new RuntimeException(op);
		}
	}
	
	/**
	 * Método que copia los elementos contenidos del elemento invocador
	 * en el elemento que se le pasa por parámetro.
	 * @param copia El elemento en el que copiar los elementos.
	 * @throws RuntimeException En caso de que la operación no sea válida.
	 */
	protected void copiarEstado(ElementoCompuesto copia) {
		super.copiarEstado(copia);
		try {
			for (int i = 0; i < _elementos.size(); i++) {
				copia.agregar((Elemento) obtenerElemento(i).copiar());
			}
		} catch (OperacionInvalida op) {
			throw new RuntimeException(op);
		}
	}
	
	private Vector<Elemento> _elementos;
	
}
