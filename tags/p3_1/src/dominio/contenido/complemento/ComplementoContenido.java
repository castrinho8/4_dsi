package dominio.contenido.complemento;

import dominio.contenido.Contenido;
import dominio.contenido.elemento.Elemento;
import dominio.usuario.Usuario;

/**
 * Clase que representa un complemento del contenido.
 */
public abstract class ComplementoContenido extends Contenido {
	
	private Contenido _contenido;
	
	/**
	 * Contructor del complemento.
	 * @param contenido Contenido al que complementa esta instancia.
	 */
	public ComplementoContenido(Contenido contenido) {
		_contenido = contenido;
	}
	
	/**
	 * Getter del usuario del contenido.
	 * Delega el metodo en el contenido complementado.
	 * @return Usuario propietario del contenido.
	 */
	public Usuario obtenerPropietario() {
		return _contenido.obtenerPropietario();
	}
	
	/**
	 * Setter del usuario del contenido.
	 * Delega el metodo en el contenido complementado.
	 * @param propietario Usuario propietario del contenido.
	 */
	public void establecerPropietario(Usuario propietario) {
		_contenido.establecerPropietario(propietario);
	}
	
	/**
	 * Getter del título del contenido.
	 * Delega el metodo en el contenido complementado.
	 * @return El título del contenido.
	 */
	public String obtenerTitulo() {
		return _contenido.obtenerTitulo();
	}
	
	/**
	 * Setter del título del contenido.
	 * Delega el metodo en el contenido complementado.
	 * @param titulo Nuevo título del contenido.
	 */
	public void establecerTitulo(String titulo) {
		_contenido.establecerTitulo(titulo);
	}
	
	/**
	 * Método que devuelve una versión imprimible del contenido.
	 * Delega el metodo en el contenido complementado.
	 * @return Un string que representa una version imprimible del contenido.
	 */
	public String obtenerVersionImprimible() {
		return _contenido.obtenerVersionImprimible();
	}
	
	/**
	 * Método que devuelve una versión imprimible de una sección del contenido.
	 * Delega el metodo en el contenido complementado.
	 * @param seccion Sección de la que se quiere obtener su version imprimible.
	 * @return Versión imprimible de la sección.
	 */
	public String obtenerVersionImprimible(String seccion) {
		return _contenido.obtenerVersionImprimible(seccion);
	}
	
	/**
	 * Método que permite obtener la raiz del contenido.
	 * Delega el metodo en el contenido complementado.
	 * @return El elemento raiz de este contenido.
	 */
	public Elemento obtenerRaiz() {
		return _contenido.obtenerRaiz();
	}
	
	/**
	 * Getter del texto del contenido.
	 * Delega el metodo en el contenido complementado.
	 * @return El texto del conenido.
	 */
	public String obtenerTexto() {
		return _contenido.obtenerTexto();
	}
	
	/**
	 * Setter del texto del contenido.
	 * Delega el metodo en el contenido complementado.
	 * @param texto Nuevo texto del contenido.
	 */
	public void establecerTexto(String texto) {
		_contenido.establecerTexto(texto);
	}
	
	/**
	 * Método que comprueba si un usuario ha valorado o no el contenido.
	 * Delega el metodo en el contenido complementado.
	 * @param usuario Usuario que se desea comprobar.
	 * @return Un booleano que indica si el usuario ha valorado o no el contenido.
	 */
	public boolean valorado(Usuario usuario) {
		return _contenido.valorado(usuario);
	}
	
	/**
	 * Método que devuelve la valoración del contenido.ç
	 * Delega el metodo en el contenido complementado.
	 * @return un entero que especifica la valoracion del contenido.
	 */
	public int obtenerValoracion() {
		return _contenido.obtenerValoracion();
	}
	
	/**
	 * Método que devuelve una copia del contenido.
	 * Delega el metodo en el contenido complementado.
	 * @return Una instancia de contenido con las mismas caracteristicas que el actual.
	 */
	public abstract Contenido copiar();
	
	/**
	 * Método que copia el estado de un contenido en este objeto.
	 * 	Delega el metodo en el contenido complementado.
	 * @param copia Contenido del que copiar el estado.
	 */
	protected void copiarEstado(ComplementoContenido copia) {
		copia._contenido = _contenido.copiar();
	}

}
