package practica1;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Clase que modela una alarma.
 *
 */
public abstract class Alarma {
	
	protected boolean estado;
	private ArrayList<Etiqueta> etiquetas;
	
	/**
	 * Constructor de la clase.
	 */
	public Alarma() {
		this.estado = false;
		this.etiquetas = new ArrayList<Etiqueta>();
	}
	
	/**
	 * Devuelve el estado de la alarma.
	 * @return El estado de la alarma.
	 */
	public boolean alerta() {
		return this.estado;
	}
	
	/**
	 * Devuelve el identificador de la alarma.
	 * @return Identificador de la alarma.	  
	 */
	public abstract String id_alarma();
	
	/**
	 * Reinicia el estado de la alarma a False.
	 */
	public void reiniciar() {
		this.estado = false;
	}
	
	/**
	 * Activa la alarma.
	 */
	public void probar() {
		this.estado = true;
	}
	
	/**
	 * Anhade una etiqueta a la alarma.
	 * @param e La etiqueta a anhadir.
	 */
	public void agregarEtiqueta(Etiqueta e) {
		this.etiquetas.add(e);
	}

	/**
	 * Obtiene las etiquetas que tiene la alarma.
	 * @return Las etiquetas.
	 */
	public Collection<Etiqueta> getEtiquetas() {
		return this.etiquetas;
	}
}
