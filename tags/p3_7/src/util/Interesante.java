package util;

/**
 * Clase que modela que un elemento es interesante para otros.
 * Delega todo su comportamiento en un gestor de interesados.
 *
 */
public abstract class Interesante {
	
	/**
	 * Añade un interesado.
	 * Delega en el Gestor de Interesados
	 * @param o El objeto interesado a añadir.
	 * @see GestorInteresados
	 */
	public void agregarInteresado(Interesado o) {
		GestorInteresados.instance().agregarInteresado(this, o);
	}
	
	/**
	 * Elimina un interesado.
	 * Delega en el Gestor de Interesados
	 * @param o El objeto interesado a eliminar.
	 * @see GestorInteresados
	 */
	public void eliminarInteresado(Interesado o) {
		GestorInteresados.instance().eliminarInteresado(this, o);
	}
	
	/**
	 * Avisa a los elementos interesados a traves del Gestor de Interesados.
	 * @see GestorInteresados
	 */
	public void avisarInteresados() {
		GestorInteresados.instance().avisarInteresados(this);
	}
}
