/**
 * 
 */
package dominio.usuario.comparador;

import dominio.usuario.Usuario;

/**
 * Comparador de usuarios por orden alfafbetico.
 */
public class ComparadorAlfabeticoUsuarios implements ComparadorUsuarios {

	/**
	 * Metodo que compara dos usuarios.
	 * @param arg0 Un usuario.
	 * @param arg1 Otro usuario.
	 * @return Devuelve 0 si los dos usuarios tienen el mismo nombre, un numero positivo se los argumentos estan
	 *  ordenados alfabeticamente y un numero negativo si no lo están.
	 */
	@Override
	public int compare(Usuario arg0, Usuario arg1) {
		return arg0.obtenerNombre().compareTo(arg1.obtenerNombre());
	}

}
