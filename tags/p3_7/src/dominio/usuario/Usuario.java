package dominio.usuario;

import java.util.Vector;

/**
 * Clase abstracta que implementa un usuario del sistema.
 * Este usuario puede especificarse como un usuario individual
 * o como un grupo compuesto de usuarios.
 * @see UsuarioIndividual
 * @see GrupoUsuarios
 */
public abstract class Usuario {
	
	/**
	 * Constructor de la clase Usuario.
	 * Inicializa el usuario con el nombre introducido por parámetro.
	 * @param nombre El nombre del usuario a crear.
	 */
	public Usuario(String nombre) {
		_nombre = nombre;
	}
	
	/**
	 * Getter del nombre de usuario.
	 * @return El nombre de usuario.
	 */
	public String obtenerNombre() {
		return _nombre;
	}
	
	/**
	 * Función que agrega un Usuario a este Usuario.
	 * Implementación por defecto del método agregar que devuelve una excepción.
	 * @param usuario El usuario a agregar.
	 * @throws RuntimeException En caso de que el usuario no pueda alojar a otros usuarios
	 */
	public void agregar(Usuario usuario) {
		throw new RuntimeException("El usuario " + this
				+ " no puede alojar otros usuarios.");
	}
	
	/**
	 * Función que elimina un usuario de este usuario.
	 * Implementación por defecto del método eliminar que devuelve una excepción.
	 * @param usuario El usuario a eliminar.
	 * @throws RuntimeException En caso de que el usuario no pueda alojar a otros usuarios
	 */
	public void eliminar(Usuario usuario) {
		throw new RuntimeException("El usuario " + this
				+ " no aloja otros usuarios.");
	}
	
	/**
	 * Función que obtiene el usuario en la posicion indicada por parámetro.
	 * Implementación por defecto del método obtenerUsuario que devuelve una excepción.
	 * @param posicion El indice de la lista en la que se encuentra el usuario.
	 * @throws RuntimeException En caso de que el usuario no pueda alojar a otros usuarios
	 */
	public Usuario obtenerUsuario(int posicion) {
		throw new RuntimeException("El usuario " + this
				+ " no aloja otros usuarios.");
	}
	
	/**
	 * Obtiene el número total de miembros pertenecientes al grupo.
	 * Implementación por defecto del método obtenerNumeroMiembros que devuelve 1.
	 * @return El numero de miembros de este usuario, en este caso 1.
	 */
	public int obtenerNumeroMiembros() {
		return 1;
	}
	
	/**
	 * Obtiene una coleccion con la lista de miembros de este usuario.
	 * Implementación por defecto que devuelve un ListadoUsuariosNulo.
	 * @return La lista de miembros.
	 */
	public ListadoUsuarios obtenerMiembros() {
		return new ListadoUsuariosNulo();
	}
	
	/**
	 * Implementación del método toString.
	 * @return El String con nombre del usuario.
	 */
	public String toString() {
		return obtenerNombre();
	}
	
	/**
	 * Método que comprueba si el usuario indicado por parámetro
	 * se encuentra contenido en el usuario que llama a la función.
	 * @param usuario El usuario a buscar.
	 * @return True en caso de que se encuentre y False en caso contrario.
	 */
	public abstract boolean contiene(Usuario usuario);
	
	/**
	 * Devuelve un vector con los miembros de este usuario.
	 * @return El vector con los miembros contenidos en el usuario.
	 */
	protected abstract Vector<Usuario> obtenerListadoMiembros();
	
	private String _nombre;
	
}
