package dominio.usuario;

import java.util.Collections;
import java.util.Vector;

import dominio.usuario.comparador.ComparadorUsuarios;

/**
 * Clase que implementa la interfaz ListadoUsuarios y que 
 * modela una colección de usuarios.
 */
public class ListadoUsuariosColeccion implements ListadoUsuarios {
	
	/**
	 * Constructor de la clase ListadoUsuariosColeccion.
	 * @param usuarios El vector de usuarios que formará la colección.
	 */
	public ListadoUsuariosColeccion(Vector<Usuario> usuarios) {
		_usuarios = usuarios;
		_posicion = 0;
	}
	
	/**
	 * Inicializa el iterador ordenando los usuarios pasados por parametro.
	 * @param usuarios El vector de ususarios que formara el listado.
	 * @param c El comparador por el cual se ordenara.
	 */
	public ListadoUsuariosColeccion(Vector<Usuario> usuarios, ComparadorUsuarios c){
		_usuarios = usuarios;
		Collections.sort(_usuarios,c);
		_posicion = 0;
	}
	
	/**
	 * Función que devuelve el siguiente usuario en la lista.
	 * @return El usuario siguiente.
	 */
	public Usuario siguiente() {
		if (_posicion < _usuarios.size()) {
			return _usuarios.elementAt(_posicion++);
		} else {
			return null;
		}
	}
	
	private Vector<Usuario> _usuarios;
	
	private int _posicion;
	
}
