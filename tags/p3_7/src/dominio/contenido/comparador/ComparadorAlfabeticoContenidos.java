package dominio.contenido.comparador;

import dominio.contenido.Contenido;

/**
 * Comparador de Contenidos por orden alfabético.
 * Compara contenidos atendiendo los titulos.
 */
public class ComparadorAlfabeticoContenidos implements ComparadorContenidos {
	
	/**
	 * Función que compara dos Contenidos.
	 * @param arg0 Un contenido.
	 * @param arg1 Otro contenido.
	 * @return Devuelve 0 si los contenidos tienen el mismo titulo, un valor positivo si los titulos de 
	 * 	arg0 y arg1 están ordenados alfabeticamente y un valor negativo en caso contrario.
	 */
	@Override
	public int compare(Contenido arg0, Contenido arg1) {
		return arg0.obtenerTitulo().compareTo(arg1.obtenerTitulo());
	}

}
