package dominio.contenido.comparador;

import java.util.Comparator;
import dominio.contenido.Contenido;

/**
 * Interfaz de comparadores de Contenidos.
 * Extiende la interfaz estandar Comparator.
 */
public interface ComparadorContenidos extends Comparator<Contenido> {}
