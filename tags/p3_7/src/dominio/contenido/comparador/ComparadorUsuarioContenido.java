/**
 * 
 */
package dominio.contenido.comparador;

import dominio.contenido.Contenido;

/**
 * Comparador de contenidos por el nombre del propietario.
 */
public class ComparadorUsuarioContenido implements ComparadorContenidos {

	/**
	 * Compara los contenidos pasados por parametro basandose en el nombre del propietario.
	 * @param arg0 Un contenido.
	 * @param arg1 Otro contenido.
	 * @return Devuelve 0 si los propietarios del los contenidos tienen el mismo nombre, un numero positivo si
	 *  los nombres de los propietarios estan ordenados alfabeticamente y un numero negativo si no lo están. 
	 */
	@Override
	public int compare(Contenido arg0, Contenido arg1) {
		return arg0.obtenerPropietario().obtenerNombre().compareTo(arg1.obtenerPropietario().obtenerNombre());
	}

}
