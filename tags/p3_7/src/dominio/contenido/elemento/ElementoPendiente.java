package dominio.contenido.elemento;

import dominio.usuario.ListadoUsuarios;
import dominio.usuario.Usuario;

/**
 * Clase que modela un elemento que se encuentra en un 
 * estado pendiente, sea de borrado ElementoPendienteBorrado o de 
 * aprobación ElementoPendienteAprobacion
 * Subclase de SituacionElemento.
 * 
 * @see SituacionElemento
 * @see ElementoPendienteBorrado
 * @see ElementoPendienteAprobacion
 * 
 */
public abstract class ElementoPendiente extends SituacionElemento {
	
	/**
	 * Constructor de la clase ElementoPendiente.
	 * @param elemento El elemento que se encuentra pendiente.
	 * @param editor El editor autorizado para el elemento.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la acción.
	 */
	public ElementoPendiente(Elemento elemento, Usuario editor)
	throws OperacionInvalida {
		_editor = editor;
		if (elemento.obtenerPropietario() != null) {
			_votantes = elemento.obtenerPropietario().obtenerNumeroMiembros();
			_aprobaciones = _votantes / 2 + 1;
			ListadoUsuarios revisores = elemento.obtenerPropietario().obtenerMiembros();
			Usuario revisor;
			while ((revisor = revisores.siguiente()) != null) {
				if (!revisor.equals(_editor)) {
					agregarMensaje(revisor, elemento);
				}
			}
		} else {
			elemento.establecerSituacion(new ElementoVisible());
		}
	}
	
	/**
	 * Método que realiza la aprobación de una revision de un elemento.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha acción.
	 * @param elemento El elemento. 
	 * @param usuario El usuario que realiza la aprobación.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la acción.
	 */
	public void aprobar(Elemento elemento, Usuario usuario)
	throws OperacionInvalida {
		if (elemento.autorizado(usuario)) {
			_votantes--;
			if (--_aprobaciones == 0) {
				eliminarMensajes(elemento);
				aprobarTransicion(elemento);
			}
		} else {
			throw new OperacionInvalida("No tiene permisos para ejecutar esta operación");
		}
	}
	
	/**
	 * Método que realiza la denegación de una revision de un elemento.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha acción.
	 * @param elemento El elemento. 
	 * @param usuario El usuario que realiza la denegación.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la acción.
	 */
	public void denegar(Elemento elemento, Usuario usuario)
	throws OperacionInvalida {
		if (elemento.autorizado(usuario)) {
			_votantes--;
			if (_votantes < _aprobaciones) {
				eliminarMensajes(elemento);
				elemento.establecerSituacion(new ElementoVisible());
			}
		} else {
			throw new OperacionInvalida("No tiene permisos para ejecutar esta operación");
		}
	}
	
	/**
	 * Método que realiza la aprobación de un elemento pendiente de aprobación.
	 * @param elemento El elemento a aprobar.
	 */
	protected abstract void aprobarTransicion(Elemento elemento)
	throws OperacionInvalida;
	
	/**
	 * Método que agrega una nueva revisión al buzon.
	 * @param revisor El revisor a agregar.
	 * @param elemento El elemento a agregar.
	 */
	protected abstract void agregarMensaje(Usuario revisor, Elemento elemento);
	
	/**
	 * Método que elimina una revisión del buzón.
	 * @param revisor El revisor a agregar.
	 * @param elemento El elemento a agregar.
	 */
	protected abstract void eliminarMensaje(Usuario revisor, Elemento elemento);
	
	/**
	 * Método que elimina todos los mensajes existentes para un elemento.
	 * @param elemento El elemento del que eliminar los mensajes.
	 */
	private void eliminarMensajes(Elemento elemento) {
		ListadoUsuarios revisores = elemento.obtenerPropietario().obtenerMiembros();
		Usuario revisor;
		while ((revisor = revisores.siguiente()) != null) {
			eliminarMensaje(revisor, elemento);
		}
	}
	
	private Usuario _editor;
	
	private int _votantes;
	
	private int _aprobaciones;
	
}
