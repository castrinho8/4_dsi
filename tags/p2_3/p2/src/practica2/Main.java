package practica2;

import practica2.cliente.*;
import practica2.servidor.*;
import practica2.usuario.*;

public class Main {

    public static void main(String argv[]) {
		Usuario gulias = new UsuarioIndividual("Gulías");
		Servidor imperio = new LogAcceso(new ProxyCache("Imperiales", "imperio.dat"));
		Servidor rebeldes = new LogAcceso(new ProxyCache("Rebeldes", "rebeldes.dat"));
		Cliente c = new ClienteGUI();
		c.añadirServidor(imperio);
		c.añadirServidor(rebeldes);
		c.conectar(gulias);
    }

}
