package practica2.usuario;

/**
 * Implementación de un usuario individual
 *
 */
public class UsuarioIndividual implements Usuario {

	/**
	 * Constructor de la clase que inicializa los atributos del usuario.
	 * @param nombre El nombre del usuario.
	 */
    public UsuarioIndividual(String nombre) {
        _nombre = nombre;
        _id = obtenerSiguienteID();
    }

    /**
     * Obtiene el siguiente ID.
     * @return El ID.
     */
    private int obtenerSiguienteID() {
        return _ultimoID++;
    }

    /**
     * Getter del nombre.
     */
    public String obtenerNombre() {
        return _nombre;
    }

    /**
     * Sobreescritura del método toString.
     */
    public String toString() {
        return obtenerNombre();
    }

	/**
	 * Getter del ID.
	 */
    public int obtenerID() {
        return _id;
    }

    /**
     * Función que autoriza a otro usuario.
     */
    public boolean autorizar(Usuario otroUsuario) {
	return (otroUsuario.obtenerID() == obtenerID());
    }

    //------------------------------------------------------------
    
    private String _nombre;
    private int _id;

    //------------------------------------------------------------
    //
    // atributo de clase, lo usamos para mantener un contador único

    private static int _ultimoID = 0;
}
