package practica2.servidor;

import practica2.usuario.Usuario;

/**
 * Implementación de una cache para el servidor real.
 */
public class ProxyCache implements Servidor{

	private String cacheClave;
	private String cacheResultado;
	private ServidorReal servidor;
	
	/**
	 * Constructor de la clase
	 * @param nombre El nombre a buscar.
	 * @param fichero El fichero diccionario.
	 */
	public ProxyCache(String nombre, String fichero) {
		this.servidor = new ServidorReal(nombre,fichero);
		this.cacheClave = null;
	}

    /**
     * Operación no implementada (establece el maestro asociado al servidor).
     *
     * @param maestro maestro propuesto para el servidor
     */
	public void establecerMaestro(Servidor maestro) {
		servidor.establecerMaestro(maestro);
	}
	
    /**
     * Operación no implementada (devuelve el maestro asociado al servidor).
     *
     * @return devuelve siempre <code>null</code>
     */
	public Servidor obtenerMaestro() {
		return servidor.obtenerMaestro();
	}
	
    /**
     * Devuelve el nombre del servidor, inicializado en el constructor.
     *
     * @return nombre del servidor
     */
	public String obtenerNombre() {
		return servidor.obtenerNombre();
	}
	
	/**
	 * Funcion que obtiene el resultado si se encuentra la clave en la cache
	 * o delega la busqueda en el servidor en caso contrario.
	 * @see ServidorReal.resolver
	 * 
     * @param usuario usuario que realiza la petición
     * @param clave clave a buscar
     * @return valor asociado a la clave. <code>null</code> si no existe
  	 */
	public String resolver(Usuario usuario, String clave) {
		if(clave == null)
			throw new IllegalArgumentException();
		
		if(!clave.equals(this.cacheClave)){
			this.cacheClave = clave;
			this.cacheResultado = servidor.resolver(usuario, clave);
		}
		
		return this.cacheResultado;
	}
	
	/**
     * Sobreescritura del metodo toString.
     * @return String con el nombre del servidor.
     */
	@Override
    public String toString() {
        return this.servidor.obtenerNombre();
    }
}
