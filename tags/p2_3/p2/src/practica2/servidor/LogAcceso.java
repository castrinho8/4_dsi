package practica2.servidor;

import practica2.usuario.Usuario;

/**
 * Clase que modela el servicio de Logs del sistema.
 * Delega en el componente los métodos, a excepción del 
 * método resolver que imprime los datos y luego realiza la llamada
 * a la función del componente.
 *
 */
public class LogAcceso extends Log{
	
	/**
	 * Constructor de la clase
	 * @param componente Servidor para el que se guarda el log.
	 */
	public LogAcceso(Servidor componente) {
		super(componente);
	}

    /**
     * Servicio de resolución de nombres, devuelve la cadena de texto
     * asociada a una clave dada.
     *
     * @param usuario usuario que realiza la petición
     * @param clave nombre a ser resuelto por el servidor
     * @return valor asociado con la clave. <code>null</code> si no se puede resolver
     */
    public String resolver(Usuario usuario, String clave){
    	
    	System.out.printf("User:%s - Clave:%s\n",usuario,clave);
    	return super.resolver(usuario, clave);
    }
    
}
