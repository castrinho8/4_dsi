package dominio.contenido.elemento;

import dominio.contenido.Contenido;

/**
 * Clase que modela un parrafo de un artículo.
 *
 */
public class Parrafo extends Elemento {
	
	/**
	 * Constructor de la clase Parrafo.
	 * @param texto Texto a añadir.
	 */
	public Parrafo(String texto) {
		super(texto);
	}
	
	/**
	 * Método que obtiene el texto del parrafo correctamente formateado para su impresión.
	 * @param seccion La sección.
	 * @return El string con la versión imprimible.
	 */
	public String obtenerVersionImprimible(String seccion) {
		return obtenerTexto();
	}
	
	/**
	 * Método que copia este elemento en otro y devuelve la copia.
	 * @return Un elemento que es copia del que llama al método.
	 */
	public Contenido copiar() {
		Contenido copia = new Parrafo(obtenerTexto());
		super.copiarEstado(copia);
		return copia;
	}
	
}
