package dominio.contenido.elemento;

import dominio.usuario.Usuario;

/**
 * Clase que modela los elementos que se encuentran visibles para el usuario.
 *
 */
public class ElementoVisible extends SituacionElemento {
	
	/**
	 * Método que inicia el estado de edición en caso de estar autorizado
	 * el usuario indicado por parámetro.
	 * @param elemento El elemento a editar.
	 * @param editor El usuario que inicia la edición.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la edición.
	 */
	public void iniciarEdicion(Elemento elemento, Usuario editor)
	throws OperacionInvalida {
		if (elemento.autorizado(editor)) {
			elemento.establecerSituacion(new ElementoEnEdicion(editor));
		} else {
			throw new OperacionInvalida("No tiene permisos para ejecutar esta operación");
		}
	}
	
	/**
	 * Método que propone el elemento pasado por parámetro para su eliminación.
	 * El usuario que se indica por parámetro debe estar autorizado para realizar dicha proposición.
	 * @param elemento El elemento a eliminar.
	 * @param editor El usuario que propone el borrado.
	 * @throws OperacionInvalida En caso de que no se disponga de permisos para realizar la edición.
	 */
	public void proponerBorrado(Elemento elemento, Usuario editor)
	throws OperacionInvalida {
		if (elemento.autorizado(editor)) {
			elemento.establecerSituacion(new ElementoPendienteBorrado(elemento,	editor));
			elemento.aprobar(editor);
		} else {
			throw new OperacionInvalida("No tiene permisos para ejecutar esta operación");
		}
	}
	
	/**
	 * Método que devuelve el codigo que indica la situación en la que se encuentra
	 * el elemento.
	 * @param elemento El elemento.
	 * @return El codigo de situacion, en este caso "-"
	 */
	public String codigoSituacion(Elemento elemento) {
		return "-";
	}
	
	/**
	 * Método que devuelve la descripción de la situación.
	 * @return La descripción de la situación, en este caso "visible"
	 */
	protected String descripcion() {
		return "visible";
	}
	
}
