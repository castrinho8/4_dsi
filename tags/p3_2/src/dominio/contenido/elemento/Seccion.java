package dominio.contenido.elemento;

import dominio.contenido.Contenido;

/**
 * Clase que modela una sección de un artículo.
 *
 */
public class Seccion extends ElementoCompuesto {
	
	/**
	 * Constructor de la clase Seccion.
	 * @param texto El texto a añadir.
	 */
	public Seccion(String texto) {
		super(texto);
	}
	
	/**
	 * Método que agrega el elemento pasado por parámetro al elemento que 
	 * llama al método.
	 * @param elemento El elemento a agregar.
	 * @throws OperacionInvalida En caso de que este elemento no puede alojar a otros elementos.
	 */
	public void agregar(Elemento elemento) throws OperacionInvalida {
		if (elemento instanceof Cabecera) {
			throw new OperacionInvalida("Este elemento no puede alojar una cabecera");
		} else {
			super.agregar(elemento);
		}
	}
	
	/**
	 * Método que obtiene el texto de la seccion correctamente formateado para su impresión.
	 * @param seccion La sección.
	 * @return El string con la versión imprimible.
	 */
	public String obtenerVersionImprimible(String seccion) {
		return seccion + " " + obtenerTexto() + "\n\n"
		     + super.obtenerVersionImprimible(seccion);
	}
	
	/**
	 * Método que copia este elemento en otro y devuelve la copia.
	 * @return Un elemento que es copia del que llama al método.
	 */
	public Contenido copiar() {
		Seccion copia = new Seccion(obtenerTexto());
		super.copiarEstado(copia);
		return copia;
	}
	
}
