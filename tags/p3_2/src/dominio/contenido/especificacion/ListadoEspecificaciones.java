package dominio.contenido.especificacion;

/**
 * Interfaz  de una lista de Especificaciones.
 *
 */
public interface ListadoEspecificaciones {
	
	/**
	 * Función que devuelve la siguiente EspecificacionContenido de la lista.
	 * @return La EspecificacionContenido siguiente.
	 */
	public EspecificacionContenido siguiente();
	
}
