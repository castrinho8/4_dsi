package dominio.contenido.especificacion;

import dominio.contenido.Contenido;

/**
 * Interfaz de una especificacion de contenido.
 *
 */
public interface EspecificacionContenido {
	
	/**
	 * Método que comprueba si el contenido es válido.
	 * @param contenido El contenido a comprobar.
	 * @return True si el contenido es válido y False en caso contrario.
	 */
	public boolean valido(Contenido contenido);
	
	/**
	 * Método que devuelve la descripción del contenido.
	 * @return La descripcion.
	 */
	public String descripcion();
	
}
