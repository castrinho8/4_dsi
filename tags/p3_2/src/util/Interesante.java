package util;

import java.util.Vector;

/**
 * Clase que modela que un elemento es interesante para otros.
 *
 */
public abstract class Interesante {
	
	/**
	 * Constructor de la clase Interesante.
	 */
	public Interesante() {
		_interesados = new Vector<Interesado>();
	}
	
	/**
	 * Añade un interesado a la lista.
	 * @param o El objeto interesado a añadir.
	 */
	public void agregarInteresado(Interesado o) {
		_interesados.addElement(o);
	}
	
	/**
	 * Elimina un interesado de la lista.
	 * @param o El objeto interesado a eliminar.
	 */
	public void eliminarInteresado(Interesado o) {
		_interesados.removeElement(o);
	}
	
	/**
	 * Método que recorre la lista de los interesados de un
	 * interesante y los avisa a todos a través de su metodo de 
	 * actualización.
	 */
	public void avisarInteresados() {
		for (int i = 0; i < _interesados.size(); i++) {
			Interesado o = _interesados.elementAt(i);
			o.actualizar();
		}
	}
	
	private Vector<Interesado> _interesados;
	
}
