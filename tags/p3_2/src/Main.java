import dominio.GestorContenidos;
import dominio.contenido.elemento.Cabecera;
import dominio.contenido.elemento.Elemento;
import dominio.contenido.elemento.OperacionInvalida;
import dominio.contenido.elemento.Parrafo;
import dominio.contenido.elemento.Seccion;
import dominio.contenido.especificacion.CualquierContenido;
import dominio.usuario.GrupoUsuarios;
import dominio.usuario.Usuario;
import dominio.usuario.UsuarioIndividual;
import frontal.swing.Login;

/**
 * Clase Main del programa que contiene el método que se ejecuta.
 *
 */
public class Main {
	
	/**
	 * Método main del programa.
	 * @param argv parametros que se le pasan al ejecutable.
	 */
	public static void main(String argv[]) {
		
		try {
			
			//
			// Usuarios y grupos
			//
			
			Usuario laura = new UsuarioIndividual("Laura Castro");
			Usuario javier = new UsuarioIndividual("Javier París");
			Usuario victor = new UsuarioIndividual("Víctor Gulías");
			Usuario david = new UsuarioIndividual("David Cabrero");
			Usuario juan = new UsuarioIndividual("Juan Quintela");
			
			Usuario dsi = new GrupoUsuarios("DSI");
			dsi.agregar(laura);
			dsi.agregar(javier);
			dsi.agregar(victor);
			
			Usuario pf = new GrupoUsuarios("PF");
			pf.agregar(juan);
			
			Usuario tp = new GrupoUsuarios("TP");
			tp.agregar(david);
			
			Usuario dc = new GrupoUsuarios("Departamento de Computación");
			dc.agregar(dsi);
			dc.agregar(pf);
			dc.agregar(tp);
			
			Usuario fic = new GrupoUsuarios("FIC");
			fic.agregar(dc);
			
			//
			// Plantillas
			//
			
			Elemento patron = new Cabecera("Plantilla patrón de diseño GoF", victor);
			patron.agregar(new Seccion("Propósito"));
			patron.agregar(new Seccion("Motivación"));
			patron.agregar(new Seccion("Aplicabilidad"));
			patron.agregar(new Seccion("Estructura"));
			patron.agregar(new Seccion("Participantes"));
			patron.agregar(new Seccion("Colaboraciones"));
			patron.agregar(new Seccion("Consecuencias"));
			Elemento implementacionPatron = new Seccion("Implementacion");
			patron.agregar(implementacionPatron);
			implementacionPatron.agregar(new Seccion("C++"));
			implementacionPatron.agregar(new Seccion("Java"));
			GestorContenidos.instancia().agregarPlantilla(patron);
			
			Elemento tema = new Cabecera("Plantilla tema", laura);
			Elemento introduccionTema = new Seccion("Introducción");
			introduccionTema.agregar(new Parrafo("Edítame"));
			tema.agregar(introduccionTema);
			Elemento seccion1Tema = new Seccion("Sección 1");
			seccion1Tema.agregar(new Parrafo("Edítame"));
			tema.agregar(seccion1Tema);
			Elemento seccion2Tema = new Seccion("Sección 2");
			seccion2Tema.agregar(new Parrafo("Edítame"));
			tema.agregar(seccion2Tema);
			Elemento ejerciciosTema = new Seccion("Ejercicios");
			ejerciciosTema.agregar(new Parrafo("Edítame"));
			tema.agregar(ejerciciosTema);
			GestorContenidos.instancia().agregarPlantilla(tema);
			
			//
			// Contenidos
			//
			
			GestorContenidos.instancia().crearContenido(patron, dsi, "Patrón Estado (State)");
			GestorContenidos.instancia().crearContenido(patron, dsi, "Patrón Observador (Observer)");
			GestorContenidos.instancia().crearContenido(patron, dsi, "Patrón Decorador (Decorator)");
			GestorContenidos.instancia().crearContenido(tema, pf, "Lambda cálculo y reescritura");
			GestorContenidos.instancia().crearContenido(tema, pf, "Tutorial Erlang");
			GestorContenidos.instancia().crearContenido(tema, tp, "Semántica operacional");
			
			//
			// Especificaciones
			//
			
			GestorContenidos.instancia().agregarEspecificacion(new CualquierContenido());
			
			//
			// Login
			//
			
			Login login = new Login();
			login.agregarUsuario(javier);
			login.agregarUsuario(laura);
			login.agregarUsuario(victor);
			login.agregarUsuario(david);
			login.agregarUsuario(juan);
			
		} catch (OperacionInvalida e) {
			e.printStackTrace();
		}
	}
	
}
