package practica1;

/**
 * Interfaz para especificaciones de alarmas.
 * @see Alarma
 */
public interface Especificacion{

	/**
	 * Metodo que comprueba si una alarma cumple los requisitos o no.
	 * @param a La alarma a comprobar.
	 * @return El valor booleano true si cumple los requisitos y false en caso contrario.
	 */
	public boolean validar(Alarma a);
	
}
