package practica2;

import practica2.cliente.*;
import practica2.servidor.*;
import practica2.usuario.*;

public class Main {

    public static void main(String argv[]) {
		Usuario gulias = new UsuarioIndividual("Gulías");
		Usuario pepe = new UsuarioIndividual("Gulías");
		Usuario manolo = new UsuarioIndividual("Gulías");
		Usuario juan = new UsuarioIndividual("Gulías");
		
		Usuario grupo = new GrupoUsuarios("Equipo C");
		((GrupoUsuarios) grupo).anhadirUsuario((UsuarioIndividual) gulias);
		((GrupoUsuarios) grupo).anhadirUsuario((UsuarioIndividual) juan);
		
		Usuario grupo2 = new GrupoUsuarios("Equipo B");
		((GrupoUsuarios) grupo2).anhadirUsuario((UsuarioIndividual) manolo);
		((GrupoUsuarios) grupo2).anhadirUsuario((UsuarioIndividual) pepe);
		
		Servidor imperio = new ControlAcceso(new LogAcceso(new ProxyCache("Imperiales", "imperio.dat")),grupo);
		Servidor rebeldes = new LogAcceso(new ControlAcceso(new ProxyCache("Rebeldes", "rebeldes.dat"),grupo2));
		Cliente c = new ClienteGUI();
		c.añadirServidor(imperio);
		c.añadirServidor(rebeldes);
		c.conectar(gulias);
    }

}
