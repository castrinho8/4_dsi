package practica2.servidor;

import practica2.usuario.Usuario;

/**
 * Clase que implementa el control de acceso de un usuario al sistema.
 */
public class ControlAcceso extends Extension {

	private Usuario usuario_autorizado;

	/**
	 * Constructor de la clase.
	 * @param componente El servidor sobre el que realizar el control.
	 * @param user_autorizado El usuario que va a poder acceder.
	 */
	public ControlAcceso(Servidor componente,Usuario user_autorizado) {
		super(componente);
		this.usuario_autorizado = user_autorizado;
	}

    /**
     * Servicio de resolución de nombres, devuelve la cadena de texto
     * asociada a una clave dada o "-- Acceso No Autorizado --" en caso de no estar autorizado.
     *
     * @param usuario usuario que realiza la petición
     * @param clave nombre a ser resuelto por el servidor
     * @return valor asociado con la clave. "-- Acceso No Autorizado --" si no se puede resolver
     */
	@Override
	public String resolver(Usuario usuario, String clave){
		if(this.usuario_autorizado.autorizar(usuario)){
			return super.resolver(usuario, clave);
		}
		return "-- Acceso No Autorizado --";
	}

}
