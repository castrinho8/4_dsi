package practica2.servidor;

import practica2.usuario.Usuario;

/**
 * Clase que modela el servicio de Logs del sistema.
 */
public class LogAcceso extends Extension{
	
	/**
	 * Constructor de la clase
	 * @param componente Servidor para el que se guarda el log.
	 */
	public LogAcceso(Servidor componente) {
		super(componente);
	}

    /**
     * Servicio de resolucion de nombres, devuelve la cadena de texto
     * asociada a una clave dada e imprime ambos valores.
     *
     * @param usuario usuario que realiza la peticion
     * @param clave nombre a ser resuelto por el servidor
     * @return valor asociado con la clave. "-- Acceso No Autorizado --" si no se puede resolver
     */
    public String resolver(Usuario usuario, String clave){	
    	System.out.printf("User:%s - Clave:%s\n",usuario,clave);
    	return super.resolver(usuario, clave);
    }
    
}
